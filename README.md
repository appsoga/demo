Demo Web Application
===

Spring boot 2.2를 사용하여 웹어플을 작성하기 위한 기본적인 기능을 포함하는 템플릿 프로젝트 입니다. 출력파일은 기본으로 `.war`파일이지만 `pom.xml`에서 `jar`로 변경하는 경우 standalong web application으로 구동이 가능합니다.

실행환경
---

자바만 설치되어 있는 시스템이고 메모리 용량이 `2Gbytes`이상이면 어디에서든지 어플 구동이 가능하다. 만약 데이터베이스를 포함하여 설치하는 경우는 메모리의 용량을 크게 잡아야 한다. 대용량의 데이터베이스 처리가 필요하다면 CPU의 성능도 무시하지 못한다.

실행에 필요한 환경:

 * Database: MySql 5.7

WAS를 사용하는 실행환경:

* JVM: openjdk-8-jre-headless
* MEM: 2GB over
* apache tomcat-8.0.45 - 웹인터페이스를 통해 배포하기 편리한 버전

>톰켓의 버전은 8.5이상은 manager 사용이 불편하므로 개발시에는 8.0버전으로 개발하는 것이 원할 함.

독립적인 실행 환경:

* JVM: openjdk-8-jre-headless
* MEM: 1GB over
* 실행명령은 `java -jar demo-0.0.1.jar --server.port=80`과 같은 형식으로 실행하면 된다.

>독립실행인 경우 기본 포트는 `80`을 사용하며 [http://localhost](http://localhost/) 주소에서 확인이 가능하다.

random string generator: <https://www.baeldung.com/java-random-string>