package sangmok.util.jstree;

@lombok.Data
@lombok.EqualsAndHashCode(callSuper = true)
@lombok.ToString(callSuper = true)
public class JsTreeRequest extends JsTreeData {

    public void setId(Integer id) {
        super.setId(String.valueOf(id));
    }

    public void setId(String id) {
        super.setId(id);
    }

}