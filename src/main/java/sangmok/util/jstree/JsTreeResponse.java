package sangmok.util.jstree;

@lombok.Data
public class JsTreeResponse {

    private java.util.List<JsTreeData> data;

}