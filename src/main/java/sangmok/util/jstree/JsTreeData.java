package sangmok.util.jstree;

import com.example.demo.data.Organization;

@lombok.Data
public class JsTreeData {

    private static final String TYPE_DEFAULT = "default";
    //
    public static final String ICON_DEFAULT = null;
    public static final String ICON_FOLDER = "glyphicon glyphicon-folder-close";
    public static final String ICON_FOLDER_OPEN = "glyphicon glyphicon-folder-open";
    public static final String ICON_FILE = "glyphicon glyphicon-file";
    //
    private String id;
    private String text;
    private String type;
    private String icon;
    private String parent;
    private Boolean children;

    public static JsTreeData create(String id, String text, String parent) {
        JsTreeData item = new JsTreeData();
        item.setId(id);
        item.setText(text);
        item.setParent(parent);
        item.setType(JsTreeData.TYPE_DEFAULT);
        item.setIcon(JsTreeData.ICON_DEFAULT);
        return item;
    }

    public static JsTreeData create(Organization t) {
        JsTreeData item = new JsTreeData();
        item.id = t.getId().toString();
        item.text = t.getText();
        item.type = JsTreeData.TYPE_DEFAULT;
        item.icon = JsTreeData.ICON_DEFAULT;

        item.parent = t.getUpper().getId().toString();
        item.children = t.getChildren();

        return item;
    }

    public JsTreeData icon(String icon) {
        this.icon = icon;
        return this;
    }

}