package com.example.demo.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import org.hibernate.annotations.GenericGenerator;

/**
 * 조직구성원
 * 
 * 
 * <pre>
 * ORG_IDX	INT	M					
 * ORG_NO	INT	M					
 * PTT_ID	VARCHAR(12)	M	UK		SUBS_PTT_ID_UK		PTT ID
 * FMC_NO	INT	O					FMC NO
 * ORG_MEM_NM	VARCHAR(100)	M					이름
 * DEPT_NM	VARCHAR(100)	M					부서명
 * POSITION_NO	INT	M					직책
 * PHONE_NO	VARCHAR(32)	O				ex) 6564932880	Subscriber Phone number
 * EMAIL	VARCHAR(64)	O				ex) xxxx@ip-tribe.com	Subscriber Email
 * STATUS	VARCHAR(10)	M					상태
 * IMAGE_ID	INT	O					사진
 * </pre>
 * 
 * @author sangmok <appsoga@gmail.com>
 * 
 */
@lombok.Data
@lombok.EqualsAndHashCode(of = { "id" })
@JsonIgnoreProperties(ignoreUnknown = true, value = { "hibernateLazyInitializer" })
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "ORGMEMBER", uniqueConstraints = @UniqueConstraint(name = "UK_PTT_ID", columnNames = { "PTT_ID" }))
public class OrgMember {

    @Id
    @GenericGenerator(name = "tid-generator", strategy = "com.example.demo.data.identifier.TIDGenerator")
    @GeneratedValue(generator = "tid-generator", strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, length = 38)
    private String id;

    // @Id
    // @GenericGenerator(name = "assigned-sequence", strategy =
    // "com.example.demo.data.identifier.StringSequenceGenerator", parameters = {
    // @org.hibernate.annotations.Parameter(name = "sequence_name", value =
    // "hibernate_sequence"),
    // @org.hibernate.annotations.Parameter(name = "sequence_prefix", value = "OME")
    // })
    // @GeneratedValue(generator = "assigned-sequence", strategy =
    // GenerationType.SEQUENCE)
    // @Column(name = "ID", updatable = false, length = 38)
    // private String id;

    @Column(name = "ORG_MEM_NM", length = 100, nullable = false)
    String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POSITION_NO", nullable = false, foreignKey = @ForeignKey(name = "FK_ORGMEMBER_TO_ORGPOSITION"))
    OrgPosition position;

    @Column(name = "PHONE_NO", length = 32)
    String phone;

    @Column(name = "EMAIL", length = 64)
    String email;

    @Column(name = "STATUS", length = 10)
    String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_MODIFIED_ON")
    private Date lastModifiedOn;

    /**
     * Reference
     */

    @Column(name = "PTT_ID", length = 12)
    String pttId;

    @Column(name = "FMC_NO")
    Integer fmcNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IMAGE_ID", nullable = true, foreignKey = @ForeignKey(name = "FK_ORGMEMBER_TO_IMAGE"))
    OrgImage image;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_NO", nullable = false, foreignKey = @ForeignKey(name = "FK_ORGMEMBER_TO_ORG"))
    Organization org;

    // @Column(name = "DEPT_NM", length = 100, nullable = false)
    // String deptName;

}