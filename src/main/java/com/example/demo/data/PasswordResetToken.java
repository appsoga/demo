package com.example.demo.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@lombok.Data
@Entity
@Table(name = "TB_PASSWORD_RESET_TOKEN")
public class PasswordResetToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "TOKEN")
    private String token;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "MEMBER_ID", nullable = true, foreignKey = @ForeignKey(name = "FK_TOKEN_TO_MEMBER"))
    private Member member;

    // @OneToOne(targetEntity = Member.class, fetch = FetchType.EAGER)
    // @JoinColumn(nullable = false, name = "memberId")

    private Date expiresOn;

}