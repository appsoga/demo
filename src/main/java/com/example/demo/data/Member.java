/*
 * File: Member.java
 * Project: db
 * File Created: Tuesday, 22nd October 2019 7:30:15 pm
 * Author: sangmok (appsoga@gmail.com)
 * -----
 * Copyright 2019 - 2019 APPSOGA Inc.
 */
package com.example.demo.data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.example.demo.data.converter.MemberGroupConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

import org.hibernate.annotations.GenericGenerator;

@lombok.Data
@lombok.ToString(exclude = { "password" })
@Entity
@Table(name = "TB_MEMBER", uniqueConstraints = @UniqueConstraint(name = "UK_EMAIL", columnNames = { "EMAIL" }))
@SecondaryTable(name = "TB_MEMBERDETAILS", indexes = {
		@Index(name = "MEMBERDETAILS_USERNAME_IDX", columnList = "USERNAME", unique = true) }, pkJoinColumns = @PrimaryKeyJoinColumn(name = "MEMBER_ID"), foreignKey = @ForeignKey(name = "FK_MEMBERDETAILS_TO_MEMBER"))

public class Member {

	@Id
	@GenericGenerator(name = "MEMBER_ID", strategy = "com.example.demo.data.identifier.UUIDGenerator")
	@GeneratedValue(generator = "MEMBER_ID", strategy = GenerationType.IDENTITY)
	@Column(name = "ID", length = 38)
	private String id;

	@Column(name = "NAME", length = 64)
	private String name;

	@Column(name = "EMAIL", length = 64)
	private String email;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MODIFIED_ON")
	private Date lastModifiedOn;

	@Convert(attributeName = "value", converter = MemberGroupConverter.class)
	@Enumerated(EnumType.STRING)
	@Column(table = "TB_MEMBERDETAILS", name = "MGROUP", length = 18, nullable = false)
	private MemberGroup group;

	@Column(table = "TB_MEMBERDETAILS", name = "USERNAME", length = 38)
	private String username;

	@Column(table = "TB_MEMBERDETAILS", name = "PASSWORD", length = 128)
	private String password;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(table = "TB_MEMBERDETAILS", name = "PASSWORD_MODIFIED_ON")
	private Date passwordModifiedOn;

	@JsonFormat(shape = JsonFormat.Shape.BOOLEAN)
	@Column(table = "TB_MEMBERDETAILS", name = "LOCKED", nullable = false, length = 5)
	private Boolean locked;

	@JsonFormat(shape = JsonFormat.Shape.BOOLEAN)
	@Column(table = "TB_MEMBERDETAILS", name = "ENABLED", nullable = false, length = 5)
	private Boolean enabled;

	@Temporal(TemporalType.DATE)
	@Column(table = "TB_MEMBERDETAILS", name = "EXPIRES_ON")
	private Date expiresOn;

}
