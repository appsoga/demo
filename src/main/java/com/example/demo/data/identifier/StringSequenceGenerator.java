package com.example.demo.data.identifier;

import java.io.Serializable;
import java.util.Properties;

import com.example.demo.data.Organization;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.Session;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.config.spi.ConfigurationService;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

/**
 * Hibernate 내부 시퀀스를 사용하고 특정코드값을 prefix 하는 키를 발생하는 클래스
 * <p>
 * 최소 14자리 = prefix(n) + sequence(13);
 * </p>
 * 
 * <pre>
 * &#64;Id
 * &#64;GenericGenerator(name = "assigned-sequence", strategy = "com.example.demo.data.identifier.StringSequenceGenerator", parameters = {
 *                 &#64;org.hibernate.annotations.Parameter(name = "sequence_name", value = "hibernate_sequence"),
 *                 &#64;org.hibernate.annotations.Parameter(name = "sequence_prefix", value = "CTC_"), })
 * &#64;GeneratedValue(generator = "assigned-sequence", strategy = GenerationType.SEQUENCE)
 * &#64;Column(name = "ID", updatable = false, length = 38)
 * private String id;
 * 
 * &#64;Version
 * private Integer version;
 * </pre>
 * 
 * @author sangmok <appsoga@gmail.com>
 */
public class StringSequenceGenerator implements IdentifierGenerator, Configurable {

        public static final String SEQUENCE_PREFIX = "sequence_prefix";

        private static final int MAX = 9999;

        private String sequencePrefix;

        private String sequenceCallSyntax;

        private int index = 0;

        @Override
        public Serializable generate(SharedSessionContractImplementor session, Object object)
                        throws HibernateException {
                // object = save object
                if (object instanceof Organization) {
                        Organization identifiable = (Organization) object;
                        Serializable id = identifiable.getId();

                        if (id != null) {
                                return id;
                        }
                }

                long seqValue = ((Number) Session.class.cast(session).createNativeQuery(sequenceCallSyntax)
                                .uniqueResult()).longValue();
                if (index == 0 || index > MAX) {
                        java.util.Random ran = new java.util.Random();
                        index = ran.nextInt(MAX) + 1;
                }
                return String.format("%s%09d%04d", sequencePrefix, seqValue, index++);
        }

        @Override
        public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
                final JdbcEnvironment jdbcEnvironment = serviceRegistry.getService(JdbcEnvironment.class);

                final Dialect dialect = jdbcEnvironment.getDialect();

                final ConfigurationService configurationService = serviceRegistry
                                .getService(ConfigurationService.class);

                String globalEntityIdentifierPrefix = configurationService.getSetting("entity.identifier.prefix",
                                String.class, "SEQ_");

                sequencePrefix = ConfigurationHelper.getString(SEQUENCE_PREFIX, params, globalEntityIdentifierPrefix);

                final String sequencePerEntitySuffix = ConfigurationHelper.getString(
                                SequenceStyleGenerator.CONFIG_SEQUENCE_PER_ENTITY_SUFFIX, params,
                                SequenceStyleGenerator.DEF_SEQUENCE_SUFFIX);

                boolean preferSequencePerEntity = ConfigurationHelper
                                .getBoolean(SequenceStyleGenerator.CONFIG_PREFER_SEQUENCE_PER_ENTITY, params, false);

                final String defaultSequenceName = preferSequencePerEntity
                                ? params.getProperty(JPA_ENTITY_NAME) + sequencePerEntitySuffix
                                : SequenceStyleGenerator.DEF_SEQUENCE_NAME;

                sequenceCallSyntax = dialect.getSequenceNextValString(ConfigurationHelper
                                .getString(SequenceStyleGenerator.SEQUENCE_PARAM, params, defaultSequenceName));
        }

}