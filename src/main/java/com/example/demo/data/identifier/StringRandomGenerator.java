package com.example.demo.data.identifier;

import java.io.Serializable;
import java.util.Random;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * 랜덤한 문자열로 구성된 키를 발생하는 클래스, 중복된 키가 만들어 질 수도 있음.
 * 
 * @author sangmok <appsoga@gmail.com>
 * @see IdentifierGenerator
 * 
 */
public class StringRandomGenerator implements IdentifierGenerator {

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return generate();
    }

    /**
     * Generate Random Bounded String with Plain Java
     * 
     * Next – let's look at creating a more constrained random String; we're going
     * to generate a random String using lowercase alphabetic letters and a set
     * length:
     * 
     * @return
     */
    protected static String generate() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 16; // length
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

}