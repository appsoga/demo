package com.example.demo.data.identifier;

import java.io.Serializable;
import java.util.UUID;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * 랜덤 UUID 문자열을 발생시키는 클래스
 *
 * <pre>
 * &#64;Id
 * &#64;GenericGenerator(name = "uuid-genertaor", strategy = "com.example.demo.data.identifier.UUIDGenerator")
 * &#64;GeneratedValue(generator = "uuid-genertaor", strategy = GenerationType.IDENTITY)
 * &#64;Column(name = "ID", length = 38)
 * private String id;
 * </pre>
 *
 * @author sangmok <appsoga@gmail.com>
 */
public class UUIDGenerator implements IdentifierGenerator {

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return UUID.randomUUID().toString();
    }

}