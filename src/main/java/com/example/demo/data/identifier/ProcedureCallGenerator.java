package com.example.demo.data.identifier;

import java.io.Serializable;
import java.util.Properties;

import javax.persistence.ParameterMode;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.procedure.ProcedureOutputs;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

/**
 * 데이타베이스의 프로시저를 호출하여 키 생성을 해주는 클래스
 * 
 * @author sangmok <appsoga@gmail.com>
 */
public class ProcedureCallGenerator implements IdentifierGenerator, Configurable {

    public static final String SEQ_GENERATOR_PARAM_KEY = "procedureParam";

    private String procedureParam;

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        ProcedureCall procedureCall = session.createStoredProcedureCall("SEQ_GENERATOR"); // 호출할 프로시저
        procedureCall.registerParameter("InputParam", String.class, ParameterMode.IN); // 프로시저에 선언된 입력받는 변수명 등록
        procedureCall.registerParameter("ID", String.class, ParameterMode.OUT); // 프로시저에 선언된 반환하는 변수명 등록
        procedureCall.setParameter("InputParam", procedureParam); // 프로시저의 변수에 전달할 파라미터 설정
        ProcedureOutputs outputs = procedureCall.getOutputs(); // 프로시저에서 반환하는값들
        return (String) outputs.getOutputParameterValue("ID");
    }

    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) {
        // @GenericGenerator에서 넘겨준 파라미터 꺼내기
        this.procedureParam = ConfigurationHelper.getString(SEQ_GENERATOR_PARAM_KEY, params);
    }

}