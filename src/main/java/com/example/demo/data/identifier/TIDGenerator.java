package com.example.demo.data.identifier;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * 시간과 내부 시퀀스를 이용해 최대한 중복되지 않은 문자열 키를 발생하는 클래스
 * 
 * "T" + time + seq = 20
 * 
 * <pre>
 * &#64;Id
 * &#64;GenericGenerator(name = "tid-generator", strategy = "com.example.demo.data.identifier.TIDGenerator")
 * &#64;GeneratedValue(generator = "tid-generator", strategy = GenerationType.IDENTITY)
 * &#64;Column(name = "ID", updatable = false, length = 38)
 * private String id;
 * </pre>
 * 
 * @author sangmok <appsoga@gmail.com>
 */
public class TIDGenerator implements IdentifierGenerator {

    private static final int MAX = 999999;
    private static java.util.Random ran;
    private int index = 0;

    static {
        ran = new java.util.Random();
    }

    @Override
    public java.io.Serializable generate(SharedSessionContractImplementor session, Object object)
            throws HibernateException {
        long time = java.util.Calendar.getInstance().getTime().getTime();
        if (index == 0 || index > MAX) {
            index = ran.nextInt(MAX) + 1;
        }
        return String.format("%s%013d%06d", "T", time, index++);
    }

}