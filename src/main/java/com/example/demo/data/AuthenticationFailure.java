package com.example.demo.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@lombok.Data
@Entity
@Table(name = "TB_AUTH_FAIL")
public class AuthenticationFailure {

    @Id
    @Column(name = "USERNAME", length = 30)
    private String username;

    @Column(name = "CNT", length = 9)
    private Integer count;

}