package com.example.demo.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 구성원 사진
 * 
 * @author sangmok <appsoga@gmail.com>
 */
@lombok.Data
@lombok.EqualsAndHashCode(of = { "id" })
@lombok.ToString(exclude = { "memberId" })
@Entity
@Table(name = "ORGIMAGE")
public class OrgImage {

    @Id
    @TableGenerator(name = "IMG_SEQ", table = "TB_SEQUENCE")
    @GeneratedValue(generator = "IMG_SEQ", strategy = GenerationType.TABLE)
    @Column(name = "IMG_NO", updatable = false, length = 38)
    private Integer id;

    @Column(name = "IMG_NM", length = 64, nullable = false)
    String name;

    @Column(name = "IMG_SIZE", nullable = false)
    String size;

    @Column(name = "IMG_LOCATION", length = 128, nullable = false)
    String location;

    @Column(name = "IMG_DESC", length = 128)
    String desc;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "MEMBER_ID", nullable = true)
    OrgMember memberId;

}