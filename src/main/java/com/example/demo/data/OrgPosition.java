package com.example.demo.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 조직 직책
 * 
 * <pre>
 * COMP_CODE VARCHAR(12) M 사업장 코드 
 * POSITION_NO INT M 직책 ID 
 * POSITION_NM VARCHAR(32) M 직책명 
 * POSISTION_ORDER SMALLINT M 순서
 * </pre>
 * 
 * @author sangmok <appsoga@gmail.com>
 * 
 */
@lombok.Data
@lombok.EqualsAndHashCode(of = { "id" })
@JsonIgnoreProperties(ignoreUnknown = true, value = { "hibernateLazyInitializer" })
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "ORGPOSITION")
public class OrgPosition {

    @Id
    @TableGenerator(table = "TB_SEQUENCE", name = "POSITION_SEQ")
    @GeneratedValue(generator = "POSITION_SEQ", strategy = GenerationType.TABLE)
    @Column(name = "POSITION_NO", updatable = false, length = 38)
    private Integer id;

    @Column(name = "POSITION_NM")
    private String name;

    @Column(name = "POSITION_ORDER")
    private Integer order;

    @Column(name = "COMP_CODE", length = 12, nullable = true)
    private String compCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORG_NO", nullable = false, foreignKey = @ForeignKey(name = "FK_ORGPOSITION_TO_ORG"))
    private Organization org;

}