package com.example.demo.data.specs;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.example.demo.data.OrgMember;

import org.springframework.data.jpa.domain.Specification;

public class OrgMemberSpecs {

	public static Specification<OrgMember> equalOrg(String orgId) {
		return new Specification<OrgMember>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<OrgMember> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("org").get("id"), orgId);
			}
		};
	}

	public static Specification<OrgMember> likeName(String value) {
		return new Specification<OrgMember>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<OrgMember> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.like(root.get("name"), value);
			}
		};
	}

}
