package com.example.demo.data.specs;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.example.demo.data.Organization;

import org.springframework.data.jpa.domain.Specification;

public class OrganizationSpecs {

	public static Specification<Organization> equalId(String id) {
		return new Specification<Organization>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Organization> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("id"), id);
			}
		};
	}

	public static Specification<Organization> equalUpper(Organization upper) {
		return new Specification<Organization>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Organization> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("upper"), upper);
			}
		};
	}

	public static Specification<Organization> isNullUpper() {
		return new Specification<Organization>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Organization> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.isNull(root.get("upper"));
			}
		};
	}

	public static Specification<Organization> equalUpperId(String value) {
		return new Specification<Organization>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Organization> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("upper").get("id"), value);
			}
		};
	}

	public static Specification<Organization> likeName(String value) {
		return new Specification<Organization>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<Organization> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.like(root.get("name"), value);
			}
		};
	}

}
