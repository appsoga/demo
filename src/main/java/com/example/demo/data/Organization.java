package com.example.demo.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 조직 (회사, 사업장)
 * 
 * <pre>
 * ORG_IDX	INT	M	PK			SEQUENCE	조직 IDX
 * ORG_NO	INT	M	PK				조직 번호
 * COMP_CODE	VARCHAR(12)	M					사업장 코드 (사업장 코드 변경 가능)
 * ORG_NM	VARCHAR(32)	M					조직이름
 * ORG_LEVEL	SMALLINT	M					조직레벨
 * UPPER_ORG_NO	INT	O					상위조직 번호
 * ORG_DESC	VARCHAR(100)	O					조직설명
 * ORG_DIS_ORDER	SMALLINT	M					표시순서
 * </pre>
 * 
 * @author sangmok <appsoga@gmail.com>
 * 
 */
@lombok.Data
@lombok.EqualsAndHashCode(of = { "id" })
@lombok.ToString(exclude = { "upper", "childs", "members" })
@JsonIgnoreProperties(ignoreUnknown = true, value = { "childs", "members", "hibernateLazyInitializer" })
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "ORGANIZATION")
public class Organization {

    @Id
    @TableGenerator(table = "TB_SEQUENCE", name = "ORG_SEQ")
    @GeneratedValue(generator = "ORG_SEQ", strategy = GenerationType.TABLE)
    @Column(name = "ORG_NO", updatable = false, length = 38)
    private Integer id;

    @Column(name = "ORG_NM", length = 32, nullable = false)
    private String text;

    @Column(name = "ORG_LEVEL", length = 4, nullable = false)
    private Integer level;

    @Column(name = "ORG_DESC", length = 100)
    private String desc;

    @Column(name = "ORG_DIS_ORDER", length = 8, nullable = false)
    private Integer order;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "UPPER_ORG_NO", nullable = true)
    private Organization upper;

    @OneToMany(mappedBy = "upper", fetch = FetchType.LAZY)
    private java.util.Collection<Organization> childs;

    @OneToMany(mappedBy = "org", fetch = FetchType.LAZY)
    private java.util.Collection<OrgMember> members;

    @Column(name = "COMP_CODE", length = 12, nullable = false)
    private String compCode;

    /**
     * JsTree::upper.id
     */
    @Transient
    private String parent;
    @Transient
    private String type;

    public Object getParent() {
        if (this.parent == null && this.getUpper() != null)
            this.parent = this.getUpper().getId().toString();
        return this.parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
        if (parent.equals("#"))
            return;
        this.upper = new Organization();
        this.upper.setId(Integer.parseInt(parent));
    }

    public String getType() {
        this.type = (this.parent == null || this.parent.equals("#")) ? "root" : "default";
        return this.type;
    }

    /**
     * JsTree::childs.size
     */
    public Boolean getChildren() {
        if (this.getChilds() != null)
            return this.getChilds().size() > 0 ? true : false;
        return false;
    }

}