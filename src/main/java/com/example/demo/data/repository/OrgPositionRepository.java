package com.example.demo.data.repository;

import com.example.demo.data.OrgPosition;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OrgPositionRepository
		extends JpaRepository<OrgPosition, Integer>, JpaSpecificationExecutor<OrgPosition> {

}
