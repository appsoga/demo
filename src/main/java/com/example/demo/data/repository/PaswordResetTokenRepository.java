package com.example.demo.data.repository;

import com.example.demo.data.PasswordResetToken;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaswordResetTokenRepository
		extends JpaRepository<PasswordResetToken, Integer>, JpaSpecificationExecutor<PasswordResetToken> {

	PasswordResetToken findByToken(String token);

}
