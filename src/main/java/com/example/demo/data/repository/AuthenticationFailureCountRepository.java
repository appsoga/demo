package com.example.demo.data.repository;

import com.example.demo.data.AuthenticationFailure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AuthenticationFailureCountRepository
		extends JpaRepository<AuthenticationFailure, String>, JpaSpecificationExecutor<AuthenticationFailure> {

}
