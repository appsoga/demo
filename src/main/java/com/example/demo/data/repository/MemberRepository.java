package com.example.demo.data.repository;

import com.example.demo.data.Member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MemberRepository extends JpaRepository<Member, String>, JpaSpecificationExecutor<Member> {

	Member findOneByUsername(String username);

	boolean existsByUsername(String username);

}
