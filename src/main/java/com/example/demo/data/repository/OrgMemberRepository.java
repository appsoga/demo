package com.example.demo.data.repository;

import com.example.demo.data.OrgMember;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OrgMemberRepository extends JpaRepository<OrgMember, String>, JpaSpecificationExecutor<OrgMember> {

}
