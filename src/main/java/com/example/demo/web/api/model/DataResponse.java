package com.example.demo.web.api.model;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;

/**
 * 
 * 응답 메시지의 프레임을 만드는 클래스
 * 
 * Usage:
 * 
 * <pre>
 * return DataResponse.create() //
 *         .paging("members", 1024L, java.util.Arrays.asList(new Data()))//
 *         // .data("member", new Data()) //
 *         .message(messageSource, "message.test", null, locale)//
 *         .success();
 * </pre>
 * 
 * <pre>
 * return DataResponse.create()//
 *         .message(messageSource, "message.test", null, Locale.getDefault())//
 *         .badRequest();
 * </pre>
 * 
 * <pre>
 * return DataResponse.notImplemented();
 * </pre>
 * 
 */
@lombok.Getter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class DataResponse extends ModelMap {

    private static final long serialVersionUID = 1L;

    public static ResponseEntity<DataResponse> ok(Object data) {
        return DataResponse.ok("data", data);
    }

    public static ResponseEntity<DataResponse> ok(String name, Object data) {
        return DataResponse.create().data(name, data).success();
    }

    public static ResponseEntity<DataResponse> notImplemented() {
        DataResponse response = DataResponse.create();
        response.addAttribute("status", HttpStatus.NOT_IMPLEMENTED.value());
        if (!response.containsKey("message"))
            response.addAttribute("message", HttpStatus.NOT_IMPLEMENTED.getReasonPhrase());
        return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(response);
    }

    public static ResponseEntity<?> exception(HttpServletRequest request, Exception ex) {
        return DataResponse.create().path(request).exception(ex);
    }

    public static DataResponse create() {
        DataResponse response = new DataResponse();
        response.addAttribute("timestamp", java.util.Calendar.getInstance().getTime());
        return response;
    }

    private DataResponse() {
        ;
    }

    public DataResponse data(Object data) {
        return this.data("data", data);
    }

    public DataResponse data(String name, Object data) {
        this.put("class", data.getClass());
        this.put(name, data);
        return this;
    }

    public DataResponse paging(long totalElements, List<Object> asList) {
        return this.paging("paging", totalElements, asList);
    }

    public DataResponse paging(String name, long totalElements, List<Object> asList) {
        ModelMap map = new ModelMap();
        map.put("dataClass",
                (asList != null && asList.size() > 0) ? asList.iterator().next().getClass().getName() : "");
        map.put("totalElements", totalElements);
        map.put("data", asList);
        this.put(name, map);
        return this;
    }

    public DataResponse message(MessageSource messageSource, String code, Object[] args, Locale locale) {
        this.put("message", messageSource.getMessage(code, args, locale));
        return this;
    }

    public DataResponse path(final HttpServletRequest request) {
        if (request != null)
            this.addAttribute("path", request.getRequestURI());
        return this;
    }

    public ResponseEntity<DataResponse> success() {
        this.addAttribute("status", HttpStatus.OK.value());
        if (!this.containsKey("message"))
            this.addAttribute("message", HttpStatus.OK.getReasonPhrase());
        return ResponseEntity.ok(this);
    }

    public ResponseEntity<DataResponse> badRequest() {
        this.addAttribute("status", HttpStatus.BAD_REQUEST.value());
        this.addAttribute("error", HttpStatus.BAD_REQUEST.getReasonPhrase());
        if (!this.containsKey("message"))
            this.addAttribute("message", HttpStatus.BAD_REQUEST);
        return ResponseEntity.badRequest().body(this);
    }

    public ResponseEntity<?> exception(Exception ex) {
        this.put("status", HttpStatus.EXPECTATION_FAILED.value());
        this.put("error", HttpStatus.EXPECTATION_FAILED.getReasonPhrase());
        this.put("message", ex.getMessage() == null ? ex.getClass() : ex.getMessage());
        // response.put("trace", ex.getStackTrace());
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(this);
    }

}