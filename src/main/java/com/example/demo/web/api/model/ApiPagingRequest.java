package com.example.demo.web.api.model;

import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 * 페이징 데이타를 요청하기 위한 메시지 프레임 클래스
 * 
 * @author sangmok <appsoga@gmail.com>
 */
@lombok.Data
public class ApiPagingRequest {

	@lombok.Data
	@lombok.EqualsAndHashCode(of = { "field", "type" })
	public static class Query {
		private String type;
		private String operation;
		private String field;
		private String value;
	}

	@lombok.Data
	public static class Querys {
		java.util.List<Query> query;
		public java.util.List<Query> getQuery() { return this.query == null ? new java.util.ArrayList<Query>() : this.query; }
		public void setQuery(java.util.List<Query> query) { this.query = query; }
	}

	@lombok.Data
	public static class SortOrder {
		String field;
		@Pattern(regexp = "(desc|asc)")
		String order = "asc";
	}

	@lombok.Data
	public static class Paging {
		private Integer pageIndex = 1;
		private Integer lineCount = 12;
		private Querys querys;
		private SortOrder sort;
		public Querys getQuerys() { return (this.querys == null) ? new Querys() : this.querys; }
		public SortOrder getSortOrder() { return this.sort; }
	}

	@lombok.Data
	public static class WebApi {
		private Paging paging;
		public Paging getPaging() { return (this.paging == null) ? new Paging() : this.paging; }
		public void setPaging(Paging paging) { this.paging = paging; }
	}

	private WebApi webApi;

	public WebApi getWebApi() { return (this.webApi == null) ? new WebApi() : this.webApi; }
	public void setWebApi(WebApi webApi) { this.webApi = webApi; }

	@JsonIgnore
	public Pageable getPageable() {
		int page = getWebApi().getPaging().getPageIndex() - 1;
		int size = getWebApi().getPaging().getLineCount();
		return PageRequest.of(page, size);
	}

	public Querys getQuerys() {	return this.getWebApi().getPaging().getQuerys(); }
	public SortOrder getSort() { return this.getWebApi().getPaging().getSort(); }

}
