package com.example.demo.web.api.model;

/**
 * 데이터 처리를 요청하기 위한 프레임 클래스
 * 
 * @param <T>
 * @author sangmok <appsoga@gmail.com>
 */
@lombok.Data
public class ApiRequest<T> {
	@lombok.Data
	public static class WebApi<T> {
		private T data;
		public void setData(T data) { this.data = data; }
		public T getData() { return this.data; }
	}
	private WebApi<T> webApi;
	public WebApi<T> getWebApi() { return this.webApi == null ? new WebApi<T>() : this.webApi; }
	public T getData() { return this.webApi.data; }
}
