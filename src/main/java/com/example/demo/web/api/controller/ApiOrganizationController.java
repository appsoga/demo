package com.example.demo.web.api.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import com.example.demo.data.Organization;
import com.example.demo.service.OrganizationService;
import com.example.demo.web.api.model.ApiPagingRequest;
import com.example.demo.web.api.model.ApiPagingRequest.Query;
import com.example.demo.web.api.model.ApiRequest;
import com.example.demo.web.api.model.ApiResponse;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 조직구성정보(organization)
 * 
 * @author sangmok <appsoga@gmail.com>
 */
@RestController
@RequestMapping(path = "api/organizations")
public class ApiOrganizationController {

    private static final String ROOT_ID = "1";

    @Autowired
    private OrganizationService orgService;

    @Autowired
    private MessageSource messageSource;

    private Logger logger = LoggerFactory.getLogger(ApiOrganizationController.class);

    @PostMapping({ "tree/root" })
    public @ResponseBody ResponseEntity<?> tree_root(Locale locale,
            @RequestBody(required = false) ApiPagingRequest paging) {
        logger.debug("request paging: {}", paging);
        String value;
        if (paging == null)
            value = ROOT_ID;
        else {
            Query query = null;
            for (Query q : paging.getQuerys().getQuery()) {
                if (q.getType().equals("parent"))
                    query = q;
                if (q.getType().equals("id")) {
                    query = q;
                    break;
                }
            }
            value = (query == null || query.getValue().equals("#")) ? ROOT_ID : query.getValue();
        }
        Integer id = Integer.valueOf(value);
        Optional<Organization> opt = orgService.getOrganization(id);
        if (opt.isPresent()) {
            Organization root = opt.get();
            root.setParent("#");
            return ApiResponse.create(Arrays.asList(root), 1).success();
        }
        return ApiResponse.message(messageSource, "api-org-controller.not-found-organization",
                java.util.Arrays.asList(id).toArray(), locale).badRequest();
    }

    @PostMapping({ "tree/childs" })
    public @ResponseBody ResponseEntity<?> tree_childs(Locale locale,
            @RequestBody(required = false) ApiPagingRequest paging) {
        logger.debug("request paging: {}", paging);
        Query query = null;
        for (Query q : paging.getQuerys().getQuery()) {
            if (q.getType().equals("parent"))
                query = q;
        }
        String value = query.getValue().equals("#") ? ROOT_ID : query.getValue();
        Integer upperId = Integer.valueOf(value);
        List<Organization> list = orgService.findOrganizationsByUpperId(upperId);
        return ApiResponse.create(list, list.size()).success();
    }

    /**
     * 조직구성정보의 페이징된 목록을 가져온다.
     * 
     * @param paging
     * @return
     */
    @PostMapping({ "paging" })
    public @ResponseBody ResponseEntity<?> pagging(Locale locale,
            @RequestBody(required = false) ApiPagingRequest paging) {
        logger.debug("request paging: {}", paging);
        return orgService.findOrganizations(paging).success();
    }

    /**
     * 조직구성정보를 가져온다.
     * 
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public @ResponseBody ResponseEntity<?> get(Locale locale, @PathVariable(name = "id", required = true) Integer id) {
        logger.debug("request id is {}.", id);
        Optional<Organization> result = orgService.getOrganization(id);
        if (result.isPresent())
            return ApiResponse.ok(result);
        return ApiResponse.message(messageSource, "api-org-controller.not-found-organization", null, locale)
                .badRequest();
    }

    /**
     * 조직구성정보를 추가한다.
     * 
     * @param in
     * @return
     */
    @PostMapping
    public @ResponseBody ResponseEntity<?> add(Locale locale, @RequestBody ApiRequest<Organization> in) {
        logger.debug("request: {}.", in);
        Organization result = orgService.addOrganization(in.getData());
        return ApiResponse.ok(result);
    }

    /**
     * 조직구성정보를 수정한다.
     * 
     * @param in
     * @return
     */
    @PutMapping
    public @ResponseBody ResponseEntity<?> modify(Locale locale, @RequestBody ApiRequest<Organization> in) {
        Organization result = orgService.modifyOrganization(in.getData());
        if (result == null)
            return ApiResponse.message(messageSource, "api-org-controller.not-found-organization", null, locale)
                    .badRequest();
        return ApiResponse.ok(result);
    }

    /**
     * 조직구성정보들을 수정한다.
     * 
     * @param in
     * @return
     */
    @PutMapping("array")
    public @ResponseBody ResponseEntity<?> modifyArray(Locale locale,
            @RequestBody ApiRequest<ArrayList<Organization>> in) {
        logger.info("in: {}", in.getData());
        // TODO: 이거 쓸거면 구현해야 함.
        return ApiResponse.unimplement();
    }

    /**
     * 조직구성정보를 삭제한다.
     * 
     * @param in
     * @return
     */
    @DeleteMapping
    public @ResponseBody ResponseEntity<?> remove(Locale locale, @RequestBody ApiRequest<Organization> in) {
        Optional<Organization> opt = orgService.getOrganization(in.getData().getId());
        if (opt.isPresent()) {
            Organization read = opt.get();
            if (read.getChildren()) {
                return ApiResponse.message(messageSource, "api-org-controller.has-children", null, locale)
                        .addAttribute("action", "delete").addAttribute("class", this.getClass()).badRequest();
            }
            if (!read.getMembers().isEmpty()) {
                return ApiResponse.message(messageSource, "api-org-controller.has-members", null, locale)
                        .addAttribute("action", "delete").addAttribute("class", this.getClass()).badRequest();
            }
            orgService.removeOrganization(in.getData());
            return ApiResponse.ok();
        } else {
            return ApiResponse.message(messageSource, "api-org-controller.are-you-dummy", null, locale).badRequest();
        }

    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleStorageFileNotFound(final ConstraintViolationException ex) {
        return ApiResponse.exception(ex);
    }

    @ExceptionHandler(NoSuchMessageException.class)
    public ResponseEntity<?> handleStorageFileNotFound(final NoSuchMessageException ex) {
        return ApiResponse.exception(ex);
    }

}