package com.example.demo.web.api.controller;

import com.example.demo.service.MemberDummyService;
import com.example.demo.service.MemberDummyService.Member;
import com.example.demo.web.api.model.ApiPagingRequest;
import com.example.demo.web.api.model.ApiRequest;
import com.example.demo.web.api.model.ApiResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@CrossOrigin("*")
@RequestMapping(value = { "api/dummy/members" })
public class ApiDummyContoller {

	@Autowired
	private MemberDummyService memberService;

	private static Logger logger = LoggerFactory.getLogger(ApiDummyContoller.class);

	/**
	 * 회원목록 페이징 조회
	 * 
	 * @param contentType
	 * @param accept
	 * @param accessToken
	 * @param tranId
	 * @param worker
	 * @param host
	 * @param sr
	 * @param ucBuilder
	 * @return
	 */
	@PostMapping("paging")
	public ResponseEntity<?> inspect(@RequestHeader(value = "content-type") String contentType,
			@RequestHeader(value = "accept") String accept, @RequestHeader(value = "access_token") String accessToken,
			@RequestHeader(value = "tranId") String tranId, @RequestHeader(value = "id") String worker,
			@RequestHeader(value = "host") String host,
			//
			@RequestBody(required = false) ApiPagingRequest sr, UriComponentsBuilder ucBuilder) {

		// if (sr == null) {
		// 	sr = new ApiPagingRequest();
		// }

		logger.info("### inspect (paging) --------------");
		logger.info("content-type: {}", contentType);
		logger.info("access_token: {}", accessToken);
		logger.info("tranId: {}", tranId);
		logger.info("worker: {}", worker);
		logger.info("host: {}", host);
		logger.info("request: {}", sr);

		//
		ApiResponse mr = memberService.findAll(sr);
		return ResponseEntity.ok(mr);
	}

	/**
	 * 회원 추가
	 * 
	 * @param contentType
	 * @param accept
	 * @param accessToken
	 * @param tranId
	 * @param worker
	 * @param host
	 * @param sr
	 * @param ucBuilder
	 * @return
	 */
	@PostMapping
	public ResponseEntity<?> insert(@RequestHeader(value = "content-type") String contentType,
			@RequestHeader(value = "accept") String accept, @RequestHeader(value = "access_token") String accessToken,
			@RequestHeader(value = "tranId") String tranId, @RequestHeader(value = "id") String worker,
			@RequestHeader(value = "host") String host,
			//
			@RequestBody ApiRequest<Member> sr, UriComponentsBuilder ucBuilder) {

		logger.info("### insert --------------");
		logger.info("content-type: {}", contentType);
		logger.info("access_token: {}", accessToken);
		logger.info("tranId: {}", tranId);
		logger.info("worker: {}", worker);
		logger.info("host: {}", host);
		logger.info("request: {}", sr);

		Member e1 = new Member();
		BeanUtils.copyProperties(sr.getData(), e1);
		//
		e1 = memberService.createMember(e1);
		//
		return ApiResponse.ok(e1);
	}

	/**
	 * 회원 조회
	 * 
	 * @param contentType
	 * @param accept
	 * @param accessToken
	 * @param tranId
	 * @param worker
	 * @param host
	 * @param id
	 * @return
	 */
	@GetMapping(path = "{id}")
	public ResponseEntity<?> selectOne(@RequestHeader(value = "content-type") String contentType,
			@RequestHeader(value = "accept") String accept, @RequestHeader(value = "access_token") String accessToken,
			@RequestHeader(value = "tranId") String tranId, @RequestHeader(value = "id") String worker,
			@RequestHeader(value = "host") String host,
			//
			@PathVariable(value = "id") Integer id) {

		logger.info("### selectOne --------------");
		logger.info("content-type: {}", contentType);
		logger.info("access_token: {}", accessToken);
		logger.info("tranId: {}", tranId);
		logger.info("worker: {}", worker);
		logger.info("host: {}", host);
		logger.info("request: rid: {}", id);

		Member e1 = memberService.findMember(new Member(id));
		return ApiResponse.ok(e1);
	}

	/**
	 * 회원 수정
	 * 
	 * @param contentType
	 * @param accept
	 * @param accessToken
	 * @param tranId
	 * @param worker
	 * @param host
	 * @param id
	 * @param sr
	 * @param ucBuilder
	 * @return
	 */
	@PutMapping
	public ResponseEntity<?> update(@RequestHeader(value = "content-type") String contentType,
			@RequestHeader(value = "accept") String accept, @RequestHeader(value = "access_token") String accessToken,
			@RequestHeader(value = "tranId") String tranId, @RequestHeader(value = "id") String worker,
			@RequestHeader(value = "host") String host,
			//
			@RequestBody ApiRequest<Member> sr, UriComponentsBuilder ucBuilder) {

		logger.info("### update --------------");
		logger.info("content-type: {}", contentType);
		logger.info("access_token: {}", accessToken);
		logger.info("tranId: {}", tranId);
		logger.info("worker: {}", worker);
		logger.info("host: {}", host);
		logger.info("request: {}", sr);
		//
		if (sr.getData() == null)
			return ResponseEntity.badRequest().build();

		Member read = memberService.findMember(sr.getData());
		BeanUtils.copyProperties(sr.getData(), read);
		read = memberService.modifyMember(read);
		return ApiResponse.ok(read);
	}

	/**
	 * 회원 삭제
	 * 
	 * @param contentType
	 * @param accept
	 * @param accessToken
	 * @param tranId
	 * @param worker
	 * @param host
	 * @param id
	 * @param sr
	 * @param ucBuilder
	 * @return
	 */
	@DeleteMapping
	public ResponseEntity<?> delete(@RequestHeader(value = "content-type") String contentType,
			@RequestHeader(value = "accept") String accept, @RequestHeader(value = "access_token") String accessToken,
			@RequestHeader(value = "tranId") String tranId, @RequestHeader(value = "id") String worker,
			@RequestHeader(value = "host") String host,
			//
			@RequestBody ApiRequest<Member> sr, UriComponentsBuilder ucBuilder) {

		logger.info("### delete --------------");
		logger.info("content-type: {}", contentType);
		logger.info("access_token: {}", accessToken);
		logger.info("tranId: {}", tranId);
		logger.info("worker: {}", worker);
		logger.info("host: {}", host);
		logger.info("request: {}", sr);

		if (sr.getData() == null)
			return ResponseEntity.badRequest().build();

		Member e1 = memberService.findMember(sr.getData());
		memberService.removeMember(e1);

		return ApiResponse.ok();
	}

}
