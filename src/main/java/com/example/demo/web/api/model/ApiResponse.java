package com.example.demo.web.api.model;

import javax.validation.ConstraintViolationException;

import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * API응답 메시지의 프레임을 만드는 클래스
 * 
 * <p>
 * Usage: success
 * </p>
 * 
 * <pre>
 * return ApiResponse.create(list, list.size()).success();
 * </pre>
 * <p>
 * Usage: bad request with messageSrouce
 * </p>
 * 
 * <pre>
 * return ApiResponse.message(messageSource, "api-org-controller.not-found-organization",
 * 		java.util.Arrays.asList(id).toArray(), locale).badRequest();
 * </pre>
 * 
 * @author sangmok <appsoga@gmail.com>
 */
public class ApiResponse {

	private static final String SUCCESS = "success";
	private static final String BAD_REQUEST = "bad request";

	private static final String KEY_STATUS = "status";
	private static final String KEY_MESSAGE = "message";
	private static final String KEY_DATA = "data";
	private static final String KEY_TOTAL_COUNT = "totalCnt";
	private static final String KEY_LIST = "list";
	private static final String KEY_TRACE = "trace";

	private java.util.LinkedHashMap<String, Object> webApi;

	public static ResponseEntity<?> ok() {
		return ApiResponse.ok(SUCCESS);
	}

	public static ResponseEntity<?> ok(Object payload) {
		return ResponseEntity.ok().body(ApiResponse.create(payload).addAttribute(KEY_STATUS, HttpStatus.OK.value())
				.addAttribute(KEY_MESSAGE, SUCCESS));
	}

	public static ResponseEntity<?> ok(java.util.Collection<?> payload, long totalCount) {
		return ResponseEntity.ok().body(ApiResponse.create(payload, totalCount)
				.addAttribute(KEY_STATUS, HttpStatus.OK.value()).addAttribute(KEY_MESSAGE, SUCCESS));
	}

	public static ResponseEntity<?> exception(Exception ex) {
		return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(ApiResponse.create(ex));
	}

	public static ResponseEntity<?> unimplement() {
		return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
				ApiResponse.create("not implemented").addAttribute(KEY_STATUS, HttpStatus.NOT_IMPLEMENTED.value()));
	}

	public static ApiResponse create(Object payload) {
		ApiResponse result = new ApiResponse();
		result.addAttribute("timestamp", java.util.Calendar.getInstance().getTime());
		result.addAttribute("dataClass", payload == null ? "" : payload.getClass());
		if (payload instanceof String) {
			result.addAttribute(KEY_MESSAGE, payload);
		} else if (payload instanceof ConstraintViolationException) {
			ConstraintViolationException ex = (ConstraintViolationException) payload;
			result.addAttribute(KEY_MESSAGE, ex.getMessage());
			result.addAttribute("originalMessage", ex.getCause().getMessage());
		} else if (payload instanceof DataIntegrityViolationException) {
			DataIntegrityViolationException ex = (DataIntegrityViolationException) payload;
			result.addAttribute(KEY_MESSAGE, ex.getMessage());
			result.addAttribute("originalMessage", ex.getRootCause().getMessage());
		} else if (payload instanceof Exception) {
			Exception ex = (Exception) payload;
			result.addAttribute(KEY_STATUS, HttpStatus.EXPECTATION_FAILED.value());
			result.addAttribute("error", HttpStatus.EXPECTATION_FAILED.toString());
			result.addAttribute(KEY_MESSAGE, ex.getMessage());
			result.addAttribute(KEY_TRACE, ex);
		} else {
			result.addAttribute(KEY_DATA, payload);
		}
		return result;
	}

	public static ApiResponse create(String name, java.util.Collection<?> payload, long totalCount) {
		ApiResponse result = new ApiResponse();
		result.addAttribute("timestamp", java.util.Calendar.getInstance().getTime());
		result.addAttribute("dataClass",
				(payload != null && payload.size() > 0) ? payload.iterator().next().getClass().getName() : "");
		result.addAttribute(KEY_TOTAL_COUNT, totalCount);
		result.addAttribute(name, payload);
		return result;
	}

	public static ApiResponse create(java.util.Collection<?> payload, long totalCount) {
		return ApiResponse.create(KEY_LIST, payload, totalCount);
	}

	public static ApiResponse message(MessageSource messageSource, String code, Object[] args,
			java.util.Locale locale) {
		if (messageSource == null)
			return new ApiResponse();
		try {
			String message = messageSource.getMessage(code, args, locale);
			return ApiResponse.create(message);
		} catch (Exception ex) {
			ex.printStackTrace();
			return ApiResponse.create(ex);
		}
	}

	private ApiResponse() {
		this.webApi = new java.util.LinkedHashMap<String, Object>();
	}

	public java.util.Map<String, Object> getWebApi() {
		return this.webApi;
	}

	public ApiResponse addAttribute(String name, Object object) {
		this.webApi.put(name, object);
		return this;
	}

	public ResponseEntity<?> badRequest() {
		if (!this.getWebApi().containsKey(KEY_STATUS))
			this.addAttribute(KEY_STATUS, HttpStatus.BAD_REQUEST.value());
		if (!this.getWebApi().containsKey(KEY_MESSAGE))
			this.addAttribute(KEY_MESSAGE, BAD_REQUEST);
		return ResponseEntity.badRequest().body(this);
	}

	public ResponseEntity<?> success() {
		if (!this.getWebApi().containsKey(KEY_STATUS))
			this.addAttribute(KEY_STATUS, HttpStatus.OK.value());
		if (!this.getWebApi().containsKey(KEY_MESSAGE))
			this.addAttribute(KEY_MESSAGE, SUCCESS);
		return ResponseEntity.ok().body(this);
	}

}