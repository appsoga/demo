package com.example.demo.web.api;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ApiAspect {

    Logger logger = LoggerFactory.getLogger(ApiAspect.class);

    @Around("execution(* com.example.demo.service.MemberService.*(..))")
    public Object logging(ProceedingJoinPoint jp) throws Throwable {

        logger.info("start - {}.{}", jp.getSignature().getDeclaringTypeName(), jp.getSignature().getName());
        Object result = jp.proceed();
        logger.info("finished - {}.{}", jp.getSignature().getDeclaringTypeName(), jp.getSignature().getName());
        return result;
    }

    // 표현식: https://jeong-pro.tistory.com/171
    @Around("execution(* com.example.demo.web.api.controller..*.*(..))")
    public Object check(ProceedingJoinPoint jp) throws Throwable {

        logger.info("start - {}.{}", jp.getSignature().getDeclaringTypeName(), jp.getSignature().getName());

        // // print args.
        // for (Object obj : jp.getArgs()) {
        // logger.info("param: {}", obj);
        // }

        // find HttpServletRequest.
        // ServletRequestAttributes srattrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        // ServletRequestAttributes srattrs = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        // HttpServletRequest request = srattrs.getRequest();

        // // print headers.
        // Enumeration<String> keys = request.getHeaderNames();
        // while (keys.hasMoreElements()) {
        // String key = keys.nextElement();
        // logger.info("header {}: {}", key, request.getHeader(key));
        // }

        // // check headers. 이거 살리면 헤더를 요구함.. ㅋㅋ
        // String token = request.getHeader("access_token");
        // if (token == null || token.isEmpty()) {
        //     return new ResponseEntity<Object>("unsupported access_token.", HttpStatus.BAD_REQUEST);
        // }

        Object result = jp.proceed();

        logger.info("finished - {}.{}", jp.getSignature().getDeclaringTypeName(), jp.getSignature().getName());
        return result;
    }

}