package com.example.demo.web.api.controller;

import com.example.demo.service.StorageService;
import com.example.demo.service.StorageService.FileUpload;
import com.example.demo.service.StorageService.StorageFileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "api/files")
public class FileUploadRestController {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadRestController.class);

    @Autowired
    private StorageService storageService;

    @PostMapping
    public ResponseEntity<?> handleFileUpload(@ModelAttribute FileUpload upload) throws Exception {

        storageService.store(upload);

        // Do processing with uploaded file data in Service layer
        return ResponseEntity.ok().build();
    }

    @PostMapping("normal")
    public ResponseEntity<String> handleFileUpload2(@RequestParam("file") MultipartFile file) throws Exception {

        String originalName = file.getOriginalFilename();
        logger.info("filename: {}", originalName);
        storageService.store(file);

        // Do processing with uploaded file data in Service layer
        return ResponseEntity.ok(originalName);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(final StorageFileNotFoundException exc) {
        return ResponseEntity.badRequest().build();
    }

}