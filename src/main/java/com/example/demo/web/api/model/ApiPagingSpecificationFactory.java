package com.example.demo.web.api.model;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute.PersistentAttributeType;

import com.example.demo.web.api.model.ApiPagingRequest.Query;

public class ApiPagingSpecificationFactory {

    public static <T> org.springframework.data.jpa.domain.Specification<T> createSpecification(
            final ApiPagingRequest request) {
        return new TablesSpecification<T>(request);
    }

    private static class TablesSpecification<T> implements org.springframework.data.jpa.domain.Specification<T> {

        private static final long serialVersionUID = 1L;
        private final ApiPagingRequest request;

        public TablesSpecification(ApiPagingRequest request) {
            this.request = request;
        }

        @Override
        public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            Predicate predicate = cb.conjunction();
            for (Query q : request.getQuerys().getQuery()) {
                String filter = q.getType();
                String filterValue = q.getValue();

                if (Boolean.class.equals(getType(root, filter))) {
                    Expression<Boolean> expression = getExpression(root, filter, Boolean.class);
                    predicate = cb.and(predicate, cb.equal(expression, Boolean.valueOf(filterValue)));
                } else if (String.class.equals(getType(root, filter))) {
                    Expression<String> stringExpression = getExpression(root, filter, String.class);
                    predicate = cb.and(predicate, cb.like(cb.lower(stringExpression),
                            Utils.getLikeFilterValue(filterValue), Utils.ESCAPE_CHAR));
                } else {
                    Expression<String> expression = getExpression(root, filter, String.class);
                    predicate = cb.and(predicate, cb.equal(expression, String.valueOf(filterValue)));
                }
            }
            return predicate;
        }

        private Class<?> getType(Root<T> root, String column) {
            // normal
            if (!column.contains(Utils.ATTRIBUTE_SEPARATOR)) {
                return root.get(column).getJavaType();
            }
            // joinedEntity.attribute
            String[] values = column.split(Utils.ESCAPED_ATTRIBUTE_SEPARATOR);
            if (root.getModel().getAttribute(values[0])
                    .getPersistentAttributeType() == PersistentAttributeType.EMBEDDED) {
                return root.get(values[0]).get(values[1]).getJavaType();
            }
            From<?, ?> from = root;
            for (int i = 0; i < values.length - 1; i++) {
                from = from.join(values[i], JoinType.LEFT);
            }
            return from.get(values[values.length - 1]).getJavaType();
        }

        private <S> Expression<S> getExpression(Root<T> root, String column, Class<S> clazz) {
            // normal
            if (!column.contains(Utils.ATTRIBUTE_SEPARATOR)) {
                return root.get(column).as(clazz);
            }
            // joinedEntity.attribute
            String[] values = column.split(Utils.ESCAPED_ATTRIBUTE_SEPARATOR);
            if (root.getModel().getAttribute(values[0])
                    .getPersistentAttributeType() == PersistentAttributeType.EMBEDDED) {
                return root.get(values[0]).get(values[1]).as(clazz);
            }
            From<?, ?> from = root;
            for (int i = 0; i < values.length - 1; i++) {
                from = from.join(values[i], JoinType.LEFT);
            }
            return from.get(values[values.length - 1]).as(clazz);
        }
    }

    private static class Utils {

        public final static String ATTRIBUTE_SEPARATOR = ".";
        public final static String ESCAPED_ATTRIBUTE_SEPARATOR = "\\.";

        public final static char ESCAPE_CHAR = '\\';

        public static String getLikeFilterValue(String filterValue) {
            return "%" + filterValue.toLowerCase().replaceAll("%", "\\\\" + "%").replaceAll("_", "\\\\" + "_") + "%";
        }

        // public static boolean isBoolean(String filterValue) {
        // return "TRUE".equalsIgnoreCase(filterValue) ||
        // "FALSE".equalsIgnoreCase(filterValue);
        // }

    }

}