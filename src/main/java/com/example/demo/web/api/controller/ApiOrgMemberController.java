package com.example.demo.web.api.controller;

import com.example.demo.data.OrgMember;
import com.example.demo.service.OrganizationService;
import com.example.demo.web.api.model.ApiPagingRequest;
import com.example.demo.web.api.model.ApiRequest;
import com.example.demo.web.api.model.ApiResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 조직구성원 (member)
 * 
 * @author sangmok <appsoga@gmail.com>
 */
@RestController
@RequestMapping(path = "api/org-members")
public class ApiOrgMemberController {

    private static Logger logger = LoggerFactory.getLogger(ApiOrgMemberController.class);

    @Autowired
    private OrganizationService orgService;

    /**
     * 조직구성원 정보의 페이징된 목록을 가져온다.
     * 
     * @param paging
     * @return
     */
    @PostMapping({ "paging" })
    public @ResponseBody ResponseEntity<?> pagging(@RequestBody(required = false) ApiPagingRequest paging) {
        logger.debug("request paging: {}", paging);
        ApiResponse result = orgService.findMembers(paging);
        return ResponseEntity.ok(result);
    }

    /**
     * 조직구성원 정보를 가져온다.
     * 
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public @ResponseBody ResponseEntity<?> get(@PathVariable(name = "id", required = true) String id) {
        logger.debug("request id is {}.", id);
        OrgMember ro = orgService.getMember(id);
        return ApiResponse.ok(ro);
    }

    /**
     * 조직구성원 정보를 추가한다.
     * 
     * @param in
     * @return
     */
    @PostMapping
    public @ResponseBody ResponseEntity<?> add(@RequestBody ApiRequest<OrgMember> in) {
        logger.debug("request: {}.", in);
        OrgMember result = orgService.addMember(in.getData());
        return ApiResponse.ok(result);
        // return ResponseEntity.ok().build();
    }

    /**
     * 조직구성원 정보를 수정한다.
     * 
     * @param in
     * @return
     */
    @PutMapping
    public @ResponseBody ResponseEntity<?> modify(@RequestBody ApiRequest<OrgMember> in) {
        OrgMember result = orgService.modifyMember(in.getData());
        return ApiResponse.ok(result);
    }

    /**
     * 조직구성원 정보를 삭제한다.
     * 
     * @param in
     * @return
     */
    @DeleteMapping
    public @ResponseBody ResponseEntity<?> remove(@RequestBody ApiRequest<OrgMember> in) {
        orgService.removeMember(in.getData());
        return ApiResponse.ok();
    }

    @ExceptionHandler(java.lang.IllegalArgumentException.class)
    public ResponseEntity<?> handleStorageFileNotFound(final java.lang.IllegalArgumentException ex) {
        return ApiResponse.exception(ex);
    }

}