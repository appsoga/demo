package com.example.demo.web.controller;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.example.demo.data.Member;
import com.example.demo.data.repository.MemberRepository;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

@Controller
public class ExportExcelController {

    @Autowired
    private MemberRepository memberRepository;

    @RequestMapping("export/members.xlsx")
    public void export_excel(HttpServletResponse response, Model model) {

        List<Member> members = memberRepository.findAll();
        model.addAttribute("members", members);

        // export filename
        String filename = "output";

        /**
         * export
         */
        XLSTransformer transformer = new XLSTransformer();
        try {
            InputStream fis = getTemplateResource().getInputStream();
            InputStream is = new BufferedInputStream(fis);
            Workbook workbook = transformer.transformXLS(is, model.asMap());
            response.setHeader("Content-Disposition", "attachment;filename=\"" + filename + ".xlsx\"");
            OutputStream os = response.getOutputStream();
            workbook.write(os);
        } catch (ParsePropertyException | InvalidFormatException | IOException e) {
            e.printStackTrace();
        }
    }

    private ClassPathResource getTemplateResource() {
        return new ClassPathResource("templates/xlsx/members.xlsx");
    }

}