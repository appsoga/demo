package com.example.demo.web.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.example.demo.web.security.ktp.KTPAuthenticationManager;
import com.example.demo.web.security.ktp.KTPAuthenticationManager.AuthenticationRequest;
import com.example.demo.web.security.ktp.KTPUserDetails;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "login-process")
public class LoginProcessController {

    @Autowired
    KTPAuthenticationManager authenticationManager;

    @RequestMapping(method = RequestMethod.POST)
    public String login(@ModelAttribute AuthenticationRequest authenticationRequest, HttpServletRequest request)
            throws JsonProcessingException {
        HttpSession session = request.getSession();
        Optional<KTPUserDetails> opt = authenticationManager.authenticate(authenticationRequest, session);
        if (opt.isPresent()) {
            DefaultSavedRequest savedRequest = (DefaultSavedRequest) session
                    .getAttribute("SPRING_SECURITY_SAVED_REQUEST");
            if (savedRequest != null) {
                String redirectUrl = savedRequest.getRedirectUrl();
                return String.format("redirect:%s", redirectUrl);
            }
            return "redirect:app/service/dashboard.html";
        }
        return "redirect:app/login.html";
    }

}