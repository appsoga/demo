package com.example.demo.web.services;

import org.springframework.boot.context.properties.ConfigurationProperties;

@lombok.Data
@ConfigurationProperties(prefix = "ptalk")
public class EnvironmentProperties {

    @lombok.Data
    public static class Security {
        /**
         * 초기 어드민의 사용자명
         */
        String username = "admin";
        /**
         * 초기 어드민의 비밀번호, 비밀번호는 리셋을 통해서 변경할 수 있음.
         */
        String password = "password";
    }

    @lombok.Data
    public static class KTPWebApi {

        String loginUrl;

        String organizationPagingUrl;
        String organizationAddUrl;
        String organizationGetUrl;
        String organizationModifyUrl;
        String organizationRemoveUrl;
        String organizationTreeRootUrl;
        String organizationTreeChildsUrl;

        String orgMembersPagingUrl;
        String orgMembersAddUrl;
        String orgMembersGetUrl;
        String orgMembersModifyUrl;
        String orgMembersRemoveUrl;
        String orgMembersAddAllUrl;
        String orgMembersModifyAllUrl;

        String orgPositionPagingUrl;
        String orgPositionAddUrl;
        String orgPositionGetUrl;
        String orgPositionModifyUrl;
        String orgPositionRemoveUrl;

        String orgFilePagingUrl;
        String orgFileAddUrl;
        String orgFileGetUrl;
        String orgFileModifyUrl;
        String orgFileRemoveUrl;
        String orgFileDownloadUrl;

        String operatorsPagingUrl;
        String operatorsAddUrl;
        String operatorsGetUrl;
        String operatorsModifyUrl;
        String operatorsRemoveUrl;
    }

    /**
     * 초기 어드민의 사용자명과 비밀번호
     */
    Security admin;
    /**
     * 비밀번호 리셋토큰의 유효시간
     */
    Integer limitMinute = 15;
    /**
     * 환경변수 스크립트의 암호화 여부
     */
    Boolean encJavascript = true;
    /**
     * 외부로 연결하는 경우 베이스 URL
     */
    String webApiBaseUrl;
    /**
     * API 호출 URL 묶음
     */
    KTPWebApi webApi;

}