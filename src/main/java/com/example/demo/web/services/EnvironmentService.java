package com.example.demo.web.services;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.web.security.MemberDetails;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class EnvironmentService {

    @lombok.Data
    public static class Header {
        @JsonProperty(value = "content-type")
        private String contentType;
        @JsonProperty(value = "access_token")
        private String accessToken;
        @JsonProperty(value = "tranId")
        private String transId;
        @JsonProperty(value = "id")
        private String worker;
    }

    @lombok.Data
    public static class KTPEnvironment {
        private Header headers;
        private java.util.HashMap<String, String> webapi;
    }

    private static final String APPLICATION_JSON = "application/json";

    @Autowired
    private EnvironmentProperties properties;

    private ObjectMapper objectMapper = new ObjectMapper();

    public String toJavascript(HttpServletRequest request) throws JsonProcessingException {

        KTPEnvironment env = env(request);
        String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(env);
        StringBuilder sb = new StringBuilder();
        sb.append("(function (global) {");
        sb.append("\n").append("var MyEnv = global.MyEnv || (global.MyEnv = ").append(jsonString).append(");");
        sb.append("\n").append("}(this));");
        return (properties.getEncJavascript()) ? new AAEncode(sb.toString()).get() : sb.toString();
    }

    private KTPEnvironment env(HttpServletRequest request) {

        // create header continer
        Header header = new Header();
        header.setContentType(APPLICATION_JSON);
        header.setTransId(UUID.randomUUID().toString());
        // header.setWorker(request.getUserPrincipal().toString());
        // header.setAccessToken("accessToken");
        //
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("authentication: " + auth);
        if (auth.getPrincipal() instanceof MemberDetails) {
            MemberDetails ud = (MemberDetails) auth.getPrincipal();
            // header.setAccessToken(ud.getLoginResponse().getAccessToken());
            header.setWorker(ud.getUsername());
        }

        // web-api
        java.util.LinkedHashMap<String, String> map = objectMapper.convertValue(properties.getWebApi(),
                new TypeReference<java.util.LinkedHashMap<String, String>>() {
                });
        StringBuilder sb = new StringBuilder();
        if (properties.getWebApiBaseUrl() == null) {
            // sb.append(request.getScheme()).append("://").append(request.getServerName()).append(":")
            // .append(request.getServerPort()).append(request.getContextPath());
            sb.append(request.getContextPath());
        } else {
            sb.append(properties.getWebApiBaseUrl());
        }
        java.util.LinkedHashMap<String, String> webapi = new java.util.LinkedHashMap<String, String>();
        for (String key : map.keySet()) {
            if (map.get(key) != null && map.get(key).startsWith("http")) {
                webapi.put(key, map.get(key));
            } else {
                String url = sb.toString() + map.get(key);
                webapi.put(key, url);
            }
        }

        // create Environment class
        KTPEnvironment env = new KTPEnvironment();
        env.setHeaders(header);
        env.setWebapi(webapi);
        return env;
    }

    public final class AAEncode {
        /** Original JavaScript text */
        private String source;

        /** Encoded JavaScript text */
        private String encoded;

        /**
         * Creates new object and obfuscates source text given in argument.
         *
         * @param text original JavaScript source.
         */
        public AAEncode(String text) {
            if (text == null)
                return;

            String[] b = new String[] { "(c^_^o)", "(ﾟΘﾟ)", "((o^_^o) - (ﾟΘﾟ))", "(o^_^o)", "(ﾟｰﾟ)", "((ﾟｰﾟ) + (ﾟΘﾟ))",
                    "((o^_^o) +(o^_^o))", "((ﾟｰﾟ) + (o^_^o))", "((ﾟｰﾟ) + (ﾟｰﾟ))", "((ﾟｰﾟ) + (ﾟｰﾟ) + (ﾟΘﾟ))",
                    "(ﾟДﾟ) .ﾟωﾟﾉ", "(ﾟДﾟ) .ﾟΘﾟﾉ", "(ﾟДﾟ) ['c']", "(ﾟДﾟ) .ﾟｰﾟﾉ", "(ﾟДﾟ) .ﾟДﾟﾉ", "(ﾟДﾟ) [ﾟΘﾟ]" };
            StringBuilder r = new StringBuilder(
                    "ﾟωﾟﾉ= /｀ｍ´）ﾉ ~┻━┻   //*´∇｀*/ ['_']; o=(ﾟｰﾟ)  =_=3; c=(ﾟΘﾟ) =(ﾟｰﾟ)-(ﾟｰﾟ); ");

            if (text.matches("/ひだまりスケッチ×(365|３５６)\\s*来週も見てくださいね[!！]/")) {
                r.append("X=_=3; ");
                r.append("\r\n\r\n    X / _ / X < \"来週も見てくださいね!\";\r\n\r\n");
            }

            r.append("(ﾟДﾟ) =(ﾟΘﾟ)= (o^_^o)/ (o^_^o);" + "(ﾟДﾟ)={ﾟΘﾟ: '_' ,ﾟωﾟﾉ : ((ﾟωﾟﾉ==3) +'_') [ﾟΘﾟ] "
                    + ",ﾟｰﾟﾉ :(ﾟωﾟﾉ+ '_')[o^_^o -(ﾟΘﾟ)] "
                    + ",ﾟДﾟﾉ:((ﾟｰﾟ==3) +'_')[ﾟｰﾟ] }; (ﾟДﾟ) [ﾟΘﾟ] =((ﾟωﾟﾉ==3) +'_') [c^_^o];"
                    + "(ﾟДﾟ) ['c'] = ((ﾟДﾟ)+'_') [ (ﾟｰﾟ)+(ﾟｰﾟ)-(ﾟΘﾟ) ];" + "(ﾟДﾟ) ['o'] = ((ﾟДﾟ)+'_') [ﾟΘﾟ];"
                    + "(ﾟoﾟ)=(ﾟДﾟ) ['c']+(ﾟДﾟ) ['o']+(ﾟωﾟﾉ +'_')[ﾟΘﾟ]+ ((ﾟωﾟﾉ==3) +'_') [ﾟｰﾟ] + "
                    + "((ﾟДﾟ) +'_') [(ﾟｰﾟ)+(ﾟｰﾟ)]+ ((ﾟｰﾟ==3) +'_') [ﾟΘﾟ]+"
                    + "((ﾟｰﾟ==3) +'_') [(ﾟｰﾟ) - (ﾟΘﾟ)]+(ﾟДﾟ) ['c']+" + "((ﾟДﾟ)+'_') [(ﾟｰﾟ)+(ﾟｰﾟ)]+ (ﾟДﾟ) ['o']+"
                    + "((ﾟｰﾟ==3) +'_') [ﾟΘﾟ];(ﾟДﾟ) ['_'] =(o^_^o) [ﾟoﾟ] [ﾟoﾟ];"
                    + "(ﾟεﾟ)=((ﾟｰﾟ==3) +'_') [ﾟΘﾟ]+ (ﾟДﾟ) .ﾟДﾟﾉ+"
                    + "((ﾟДﾟ)+'_') [(ﾟｰﾟ) + (ﾟｰﾟ)]+((ﾟｰﾟ==3) +'_') [o^_^o -ﾟΘﾟ]+"
                    + "((ﾟｰﾟ==3) +'_') [ﾟΘﾟ]+ (ﾟωﾟﾉ +'_') [ﾟΘﾟ]; " + "(ﾟｰﾟ)+=(ﾟΘﾟ); (ﾟДﾟ)[ﾟεﾟ]='\\\\'; "
                    + "(ﾟДﾟ).ﾟΘﾟﾉ=(ﾟДﾟ+ ﾟｰﾟ)[o^_^o -(ﾟΘﾟ)];" + "(oﾟｰﾟo)=(ﾟωﾟﾉ +'_')[c^_^o];" + "(ﾟДﾟ) [ﾟoﾟ]='\\\"';"
                    + "(ﾟДﾟ) ['_'] ( (ﾟДﾟ) ['_'] (ﾟεﾟ+");

            r.append("(ﾟДﾟ)[ﾟoﾟ]+ ");

            for (int i = 0; i < text.length(); i++) {
                int n = text.charAt(i);
                StringBuilder t = new StringBuilder("(ﾟДﾟ)[ﾟεﾟ]+");
                if (n <= 127) {
                    char[] chars = Integer.toOctalString(n).toCharArray();
                    for (char c : chars)
                        t.append(b[Integer.parseInt(String.valueOf(c))]).append("+ ");
                } else {
                    String hexString = "000" + Integer.toHexString(n);
                    char[] chars = hexString.substring(hexString.length() - 4).toCharArray();
                    t.append("(oﾟｰﾟo)+ ");
                    for (char c : chars)
                        t.append(b[Integer.parseUnsignedInt(String.valueOf(c), 16)]).append("+ ");
                }
                r.append(t);
            }
            r.append("(ﾟДﾟ)[ﾟoﾟ]) (ﾟΘﾟ)) ('_');");

            this.source = text;
            this.encoded = r.toString();
        }

        //////// //////// //////// //////// //////// //////// //////// ////////

        /**
         * Returns source JavaScript text, or {@code null} if there is no such text.
         *
         * @return {@link #source} JavaScript text, or {@code null} if there is no such
         *         text
         */
        public String getSource() {
            return this.source;
        }

        /**
         * Returns encoded JavaScript text, or {@code null} if there is no such text.
         *
         * @return {@link #encoded} JavaScript text, or {@code null} if there is no such
         *         text
         */
        public String get() {
            return this.encoded;
        }
    }

}