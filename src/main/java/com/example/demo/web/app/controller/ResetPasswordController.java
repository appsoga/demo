package com.example.demo.web.app.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import com.example.demo.data.Member;
import com.example.demo.data.PasswordResetToken;
import com.example.demo.service.MemberService;
import com.example.demo.web.api.controller.UserNotFoundException;
import com.example.demo.web.security.SecurityService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping(value = "app")
@Controller
public class ResetPasswordController {

	private Logger logger = LoggerFactory.getLogger(ResetPasswordController.class);

	@Autowired
	MessageSource messageSource;

	@Autowired
	private MemberService memberService;

	@Autowired
	private SecurityService securityService;

	@Value("#{servletContext.contextPath}")
	private String contextPath;

	@RequestMapping(value = "reset-password.html", method = RequestMethod.GET)
	public void reset_password_html(Locale locale, Model model) {
		;
	}

	@RequestMapping(value = "reset-password.html", method = RequestMethod.POST)
	public String reset_password(HttpServletRequest request, @RequestParam("email") String userEmail)
			throws UserNotFoundException {

		Member user = memberService.findUserByEmail(userEmail);
		if (user == null) {
			throw new UserNotFoundException();
		}
		PasswordResetToken token = securityService.createPasswordResetTokenForUser(user);
		securityService.send(createResetTokenEmail(request, token));
		// smtpService.send(createResetTokenEmail(request, token));
		// return new ResponseEntity<String>(
		// messageSource.getMessage("message.resetPasswordEmail", null,
		// request.getLocale()), HttpStatus.OK);
		return "redirect:login.html";
	}

	/**
	 * 외부에서 비밀번호 초기화 링크타고 온 요청에 대한 처리
	 * 
	 * @param locale
	 * @param id
	 * @param token
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "change-password.html", method = RequestMethod.GET)
	public String changePassword_html(Locale locale, @RequestParam("id") String id, //
			@RequestParam("token") String token, Model model) {
		String result = securityService.validatePasswordResetToken(id, token);
		logger.debug("result: {}", result);
		if (result != null) {
			model.addAttribute("message", messageSource.getMessage("auth.message." + result, null, locale));
			return "redirect:login?lang=" + locale.getLanguage();
		}
		return "redirect:update-password.html?lang=" + locale.getLanguage();
	}

	@lombok.Data
	public static class ChangePasswordRequest {
		String password;
		String rePassword;
	}

	@RequestMapping(value = "update-password.html", method = RequestMethod.GET)
	public void update_password_html(Model model) {
		ChangePasswordRequest item = new ChangePasswordRequest();
		model.addAttribute("item", item);
	}

	@RequestMapping(value = "update-password.html", method = RequestMethod.POST)
	public String update_password(Locale locale, ChangePasswordRequest req, Model model) {
		// Member user = (Member)
		// SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		// memberService.changePassword(user.getUsername(), req.getPassword());
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		memberService.changePassword(username, req.getPassword());
		return "redirect:logout";
	}

	private SimpleMailMessage createResetTokenEmail(HttpServletRequest request, PasswordResetToken resetToken) {

		Locale locale = request.getLocale();
		Member user = resetToken.getMember();
		//
		String name = user.getName();
		String username = user.getUsername();
		String email = user.getEmail();
		StringBuilder resetLink = createChangePasswordLink(request, resetToken);
		//
		String message = messageSource.getMessage("message.resetPassword", new String[] { username, name }, locale);
		return constructEmail("Reset Password", message + " \r\n" + resetLink, email);
	}

	private SimpleMailMessage constructEmail(String subject, String body, String email) {
		SimpleMailMessage smm = new SimpleMailMessage();
		smm.setTo(email);
		smm.setSubject(subject);
		smm.setText(body);
		return smm;
	}

	private StringBuilder createChangePasswordLink(HttpServletRequest request, PasswordResetToken resetToken) {
		String token = resetToken.getToken();
		String id = resetToken.getMember().getId();
		//
		String url = request.getRequestURL().toString();
		String uri = request.getRequestURI();
		String host = url.replaceAll(uri, "");
		//
		StringBuilder sb = new StringBuilder();
		sb.append(host).append(contextPath).append("/app/change-password.html");
		sb.append("?id=").append(id).append("&token=").append(token);
		return sb;
	}

}