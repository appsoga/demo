/**
 * org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController를 대체하는 클래스, 꼭 필요하지는 않지만 필요하면 쓸 수 있다.
 * @see https://www.baeldung.com/spring-boot-custom-error-page
 */
package com.example.demo.web.app.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

// @Controller
// @RequestMapping("/error")
public class ExceptionHandlingController /*
                                          * extends org.springframework.boot.autoconfigure.web.servlet.error.
                                          * BasicErrorController
                                          */ implements ErrorController {

    private static final String ERROR_PAGE_PATH = "/error";
    private Logger logger = LoggerFactory.getLogger(ExceptionHandlingController.class);

    // @ResponseStatus(value = HttpStatus.CONFLICT, reason = "Data integrity
    // violation")
    // @ExceptionHandler(DataIntegrityViolationException.class)
    // public void conflict() {
    // // Nothing to do
    // }

    // @ExceptionHandler(Exception.class)
    // public ModelAndView handlerError(HttpServletRequest req, Exception ex) {
    // logger.error("Request: " + req.getRequestURL() + " raised " + ex);

    // ModelAndView mav = new ModelAndView();
    // mav.addObject("exception", ex);
    // mav.addObject("url", req.getRequestURL());
    // mav.setViewName("error");
    // return mav;
    // }

    @RequestMapping
    public String handleError(HttpServletRequest request, Model model) {

        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Object message = request.getAttribute(RequestDispatcher.ERROR_MESSAGE);

        model.addAttribute("status", status);
        model.addAttribute("message", message);
        model.addAttribute("url", request.getRequestURL());

        logger.debug("status: {}", status);
        if (status != null) {
            // Integer statusCode = Integer.valueOf(status.toString());
            // if (statusCode == HttpStatus.NOT_FOUND.value()) {
            // return "error-404";
            // }
            // else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
            // return "error-500";
            // }
        }
        return "error";
    }

    @Override
    public String getErrorPath() {
        return ERROR_PAGE_PATH;
    }

}