package com.example.demo.web.app.controller;

import java.util.Enumeration;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.example.demo.web.services.EnvironmentService;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping(value = "app")
@Controller
public class HomeController {

	private Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	MessageSource messageSource;

	@Autowired
	private EnvironmentService envService;

	@Value("#{servletContext.contextPath}")
	private String contextPath;

	@RequestMapping(value = "env.js", produces = "text/javascript")
	public @ResponseBody ResponseEntity<?> app_env_js(HttpServletRequest request) throws JsonProcessingException {
		String javascript = envService.toJavascript(request);
		return ResponseEntity.ok(javascript);
	}

	@RequestMapping(value = "login.html", method = RequestMethod.GET)
	public void login_html(Locale locale, HttpServletRequest request) {
		Enumeration<String> iter = request.getHeaderNames();
		while (iter.hasMoreElements()) {
			String hName = iter.nextElement();
			String hValue = request.getHeader(hName);
			logger.info("header: {}={}", hName, hValue);
		}
		// session attribute
		HttpSession session = request.getSession();
		iter = session.getAttributeNames();
		while (iter.hasMoreElements()) {
			String hName = iter.nextElement();
			Object hValue = session.getAttribute(hName);
			logger.info("session.attribute: {}={}", hName, hValue);
		}
		logger.info("locale is {}", locale);
	}

	@RequestMapping(value = "dashboard.html")
	public void dashboard_html(Locale locale, Model model) {
		;
	}

	@RequestMapping(value = "mypage.html")
	public void mypage_html(Locale locale, Model model) {
		;
	}

	@RequestMapping(value = "about.html")
	public void about_html(Locale locale, Model model) {
		;
	}

}