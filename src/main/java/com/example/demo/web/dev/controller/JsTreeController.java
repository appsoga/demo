package com.example.demo.web.dev.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("dev/jstree")
public class JsTreeController {

    private Logger logger = LoggerFactory.getLogger(JsTreeController.class);

    @RequestMapping
    public void jstree_html() {
        logger.info("accessed");
    }

}