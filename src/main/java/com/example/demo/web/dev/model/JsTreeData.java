package com.example.demo.web.dev.model;

public interface JsTreeData {

    public Object getId();

    public String getText();

    public String getType();

    public String getIcon();

    public Object getParent();

    public Boolean getChildren();

}