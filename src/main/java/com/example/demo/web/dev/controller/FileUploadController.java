package com.example.demo.web.dev.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.example.demo.service.StorageService;
import com.example.demo.service.StorageService.FileUpload;
import com.example.demo.service.StorageService.StorageFileNotFoundException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "dev/files")
public class FileUploadController {

    public static final String uploadingDir = ".data/upload/";

    private Logger logger = LoggerFactory.getLogger(FileUploadController.class);

    @Autowired
    private StorageService storageService;

    @RequestMapping(value = "form.html")
    public void form_html() {

    }

    @GetMapping(value = "upload.html")
    public void listUploadedFiles(final Model model) throws IOException {

        // model.addAttribute("files",
        // storageService.loadAll()
        // .map(path -> MvcUriComponentsBuilder
        // .fromMethodName(FileUploadController.class, "serveFile",
        // path.getFileName().toString())
        // .build().toString())
        // .collect(Collectors.toList()));
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        logger.info("Current relative path is: {}", s);

        File file = Paths.get(".").resolve(uploadingDir).toFile();
        logger.info("{}", file.getAbsolutePath());
        model.addAttribute("files", file.listFiles());
    }

    @GetMapping("/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable final String filename) {
        final Resource file = storageService.loadAsResource(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    /**
     * 멀티파일 업로드
     */
    @RequestMapping(value = "upload-files", method = RequestMethod.POST)
    public String uploadingPost(@RequestParam("uploadingFiles") MultipartFile[] uploadingFiles) throws IOException {

        Path path = Paths.get(".").resolve(uploadingDir);
        File f = path.toFile();
        if (!f.exists())
            f.mkdirs();

        for (MultipartFile uploadedFile : uploadingFiles) {
            File file = path.resolve(uploadedFile.getOriginalFilename()).toAbsolutePath().toFile();
            logger.info(file.getAbsolutePath());
            uploadedFile.transferTo(file);
        }

        return "redirect:upload.html";
    }

    @lombok.Data
    public static class Member {
        String id;
        String name;
        Integer age;
    }

    @PostMapping("upload-excel")
    public String handleExcel(@RequestParam("file") MultipartFile upload, Model model) throws IOException {

        List<Member> memberList = new ArrayList<Member>();
        XSSFWorkbook workbook = new XSSFWorkbook(upload.getInputStream());

        XSSFSheet worksheet = workbook.getSheetAt(0);
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);

            Member member = new Member();
            member.setId(row.getCell(0).getRawValue());
            member.setName(row.getCell(1).getStringCellValue());
            member.setAge(Integer.valueOf((int) row.getCell(2).getNumericCellValue()));
            memberList.add(member);
            logger.info(member.toString());
        }
        workbook.close();
        model.addAttribute("members", memberList);
        return "dev/files/upload.html";
    }

    /**
     * FileUpload 모델을 통해서 데이터를 받는다.
     * 
     * @param upload
     * @param redirectAttributes
     * @return
     * @throws StorageFileNotFoundException
     */
    @PostMapping("upload-file")
    public String handleFileUpload(@ModelAttribute FileUpload upload, final RedirectAttributes redirectAttributes)
            throws StorageFileNotFoundException {

        logger.info("upload: {}", upload);
        storageService.store(upload);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + upload.getFile().getOriginalFilename() + "!");
        return "redirect:upload.html";
    }

    /**
     * 각 속성으로 값을 받는다.
     * 
     * @param persionId
     * @param file
     * @param redirectAttributes
     * @return
     * @throws IOException
     */
    @PostMapping("normal")
    public String handleFileUpload2(@RequestParam("personId") String persionId,
            @RequestParam("file") final MultipartFile file, final RedirectAttributes redirectAttributes)
            throws IOException {

        logger.info("personId: {}", persionId);
        storageService.store(file);
        redirectAttributes.addFlashAttribute("message",
                "You successfully uploaded " + file.getOriginalFilename() + "!");

        return "redirect:files";
    }

    /**
     * 예외 처리기로 StorageService 발생하는 StorageFileNotFoundException 에 대한 예외를 처리한다.
     * 
     * @param exc
     * @return
     */
    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(final StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

}