package com.example.demo.web.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFailureBadCredentialsEventListener
        implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

    private final Logger logger = LoggerFactory.getLogger(AuthenticationFailureBadCredentialsEventListener.class);

    @Autowired
    private SecurityService securityService;

    @Override
    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {

        Object username = event.getAuthentication().getPrincipal();
        Object credentials = event.getAuthentication().getCredentials();
        logger.debug("Failed login using USERNAME [" + username + "]");
        logger.debug("Failed login using PASSWORD [" + credentials + "]");

        securityService.failure(username.toString());

    }

}