package com.example.demo.web.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationSuccessEventListener implements ApplicationListener<AuthenticationSuccessEvent> {

    private final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessEventListener.class);

    @Autowired
    private SecurityService securityService;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {

        MemberDetails member = (MemberDetails) event.getAuthentication().getPrincipal();
        // Object credentials = event.getAuthentication().getCredentials();
        logger.debug("Success login using USERNAME [" + member + "]");
        // logger.debug("Success login using PASSWORD [" + credentials + "]");

        String username = member.getUsername();
        securityService.success(username);

    }

}