package com.example.demo.web.security.ktp;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Component;

@Component
public class KTPAuthenticationManager /* implements AuthenticationManager */ {

    @lombok.Data
    public static class AuthenticationRequest {
        String username;
        String password;
    }

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    KTPUserDetailsService userDetailsService;

    public Optional<KTPUserDetails> authenticate(AuthenticationRequest authenticationRequest, HttpSession session)
            throws AuthenticationException, JsonProcessingException {
        String username = authenticationRequest.getUsername();
        String password = authenticationRequest.getPassword();

        Optional<KTPUserDetails> opt = userDetailsService.remoteLogin(username, password);
        if (opt.isPresent()) {
            // after authenticated
            userDetailsService.addDetils(username, opt.get());
            // create authenticationToken
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
                    SecurityContextHolder.getContext());
        }
        return opt;
    }

}