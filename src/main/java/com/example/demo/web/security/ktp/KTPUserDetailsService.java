package com.example.demo.web.security.ktp;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import java.util.Optional;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import kong.unirest.Unirest;

@Service
public class KTPUserDetailsService implements UserDetailsService {

    private static Logger logger = LoggerFactory.getLogger(KTPUserDetailsService.class);

    @lombok.Data
    public static class RequestModel {

        private java.util.HashMap<String, Object> webApi;

        public java.util.HashMap<String, Object> getWebApi() {
            if (this.webApi == null)
                this.webApi = new java.util.HashMap<String, Object>();
            return this.webApi;
        }

        public void addAttribute(String name, Object data) {
            this.getWebApi().put(name, data);
        }

        public Object getAttribute(String name) {
            return this.getWebApi().get(name);
        }
    }

    @lombok.Data
    public static class LoginData {
        @JsonProperty(value = "id")
        String id;

        @JsonProperty(value = "pwd")
        String password;
    }

    public static enum AdminType {
        operator
    }

    @lombok.Data
    public static class LoginResponse {
        String accessToken;
        String tokenType;
        AdminType adminType;
        java.util.Date expiresIn;
    }

    private Optional<LoginResponse> login(String id, String password) throws JsonProcessingException {
        LoginData data = new LoginData();
        data.setId(id);
        data.setPassword(encryptSHA256(password));

        System.out.println("--------RequestModel--------");
        RequestModel req = new RequestModel();
        req.addAttribute("login", data);
        System.out.println(req);

        // TODO 로그인 인증서버 연동 해야됨.

        LoginResponse response;
        Boolean enabled = false;
        if (enabled) {
            System.out.println("--------request REST api--------");
            response = Unirest.post("/ptt/web/1.0/login")//
                    .header("Content-Type", "application/json").header("accept", "application/json")
                    .header("x-api-key", "PMAK-5dd39a703408ff003c71e5b1-d99f86add9f089a12c85f0c4eb35635317")
                    .header("access_token", UUID.randomUUID().toString())//
                    .header("tranId", UUID.randomUUID().toString()) // uuid
                    .header("id", "admin").header("host", "csc.mcptt.com") //
                    .body(req).asObject(LoginResponse.class).getBody();
            response.getAccessToken();
        } else { // dummy
            response = new LoginResponse();
            response.setTokenType("Bearer");
            response.setExpiresIn(java.util.Calendar.getInstance().getTime());
            response.setAdminType(AdminType.operator);
            response.setAccessToken(
                    "eyJhbGciOiJIUzI1NiJ9.eyJtY3B0dF9pZCI6IjU1NTExMTEwMTciLCJzdWIiOiJhRWZkQTFRdVhqZVBpN25oQmZ0d1RZRjFrSy1yazFELTRRZ29kMDkzM1MwIiwiYXVkIjoiTUNQVFRfVUVfQU5EUk9JRCIsImlzcyI6ImlkbXMubWNwdHQuY29tIiwiZXhwIjoxNTc3Njg1MDI2LCJpYXQiOjE1Njk4MjI2MjZ9.px7RkeoJO1F14zpQBjj7CMpjPCIjmkcFFyA16xLOdoI");
        }
        return java.util.Optional.of(response);
    }

    private String encryptSHA256(String pwd) {

        if (pwd == null)
            return null;

        String lastPwd = null;
        try {
            byte[] bytes = pwd.getBytes("UTF-8");
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            byte[] hash = sha256.digest(bytes);

            Base64.Encoder encoder = Base64.getUrlEncoder();
            String unUrlSafeString = encoder.encodeToString(hash);
            lastPwd = unUrlSafeString.replaceAll("=", "");
            lastPwd = lastPwd.replace(System.getProperty("line.separator"), "");
            lastPwd = lastPwd.replace(System.getProperty("line.separator"), "");

            logger.debug("SHA256 Base64: " + pwd + " :" + lastPwd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lastPwd;
    }

    /**
     * userDetailsService.
     */

    private java.util.HashMap<String, KTPUserDetails> tokenMap = new java.util.HashMap<String, KTPUserDetails>();

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.tokenMap.get(username);
    }

    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    public void addDetils(String username, KTPUserDetails ktpUserDetails) {
        this.tokenMap.put(username, ktpUserDetails);
    }

    public Optional<KTPUserDetails> remoteLogin(String username, String password) throws JsonProcessingException {
        KTPUserDetails details = null;
        // remote login
        Optional<LoginResponse> opt = login(username, password);
        if (opt.isPresent()) {
            // user details
            String encPassword = passwordEncoder().encode(password);
            details = new KTPUserDetails(username, encPassword, opt.get());
            details.setAuthorities(Arrays.asList(new SimpleGrantedAuthority("ROLL_ADMINISTRATORS"),
                    new SimpleGrantedAuthority("ROLL_USER")));

            // TODO UserDetials 인증정보에 추가작업이 필요할거야.
        }
        return java.util.Optional.ofNullable(details);
    }

}