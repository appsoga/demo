package com.example.demo.web.security;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;

import com.example.demo.data.AuthenticationFailure;
import com.example.demo.data.Member;
import com.example.demo.data.PasswordResetToken;
import com.example.demo.data.repository.AuthenticationFailureCountRepository;
import com.example.demo.data.repository.MemberRepository;
import com.example.demo.data.repository.PaswordResetTokenRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityService {

    @lombok.Data
    @ConfigurationProperties(prefix = "demo.security-service")
    public static class Environment {

        /**
         * 인증 실패 제한: 이 횟수보다 인증에 실패하는 경우 계정이 잠김니다.
         */
        private Integer authFailedLimit = 10;

        /**
         * 발행된 토큰의 유효기간을 분단위로 설정할 수 있다. default: 15
         */
        private Integer passwordResetTokenExpirationMinute = 15;

        /**
         * 실제로 메일을 보낼지 여부, default: false;
         */
        private Boolean mailEnabled = false;

        private String mailSender = "";

    }

    private final Logger logger = LoggerFactory.getLogger(SecurityService.class);

    @Autowired
    private Environment env;

    // @Autowired
    // private ServletContext servletContext;

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private PaswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    private AuthenticationFailureCountRepository failureRepository;

    public void failure(String username) {
        AuthenticationFailure e1 = null;
        Optional<AuthenticationFailure> opt = failureRepository.findById(username);
        if (opt.isPresent()) {
            e1 = opt.get();
        } else {
            e1 = new AuthenticationFailure();
            e1.setUsername(username);
            e1.setCount(0);
        }
        e1.setCount(e1.getCount() + 1);
        logger.debug("failure: {}", e1);

        /**
         * 최대 실패 횟수 보다 많이 실패하면 계정을 잠근다.
         */
        if (e1.getCount() >= env.getAuthFailedLimit()) {
            // Member user = memberService.getMemberByUsername(username);
            if (memberRepository.existsByUsername(username)) {
                Member user = memberRepository.findOneByUsername(username);
                user.setLocked(true);
                memberRepository.save(user);
            }
        }

        failureRepository.save(e1);
    }

    public void success(String username) {
        if (failureRepository.existsById(username))
            failureRepository.deleteById(username);
    }

    /**
     * 비밀번호 재 설정을 위한 임티 토큰을 발급한다.
     * 
     * @param member
     * @param token
     */
    public PasswordResetToken createPasswordResetTokenForUser(Member member) {

        String token = java.util.UUID.randomUUID().toString();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, env.getPasswordResetTokenExpirationMinute());

        PasswordResetToken resetToken = new PasswordResetToken();
        resetToken.setMember(member);
        resetToken.setToken(token);
        resetToken.setExpiresOn(cal.getTime());
        resetToken = passwordResetTokenRepository.save(resetToken);
        return resetToken;
    }

    /**
     * 비밀번호 초기화 처리를 위한 토큰값 검사, 토큰이 올바르다면 해당 아이디로 임시 로그인 처리
     * <https://www.baeldung.com/spring-security-registration-i-forgot-my-password>
     */
    public String validatePasswordResetToken(String id, String token) {
        PasswordResetToken passToken = passwordResetTokenRepository.findByToken(token);
        if (passToken == null)
            return "invalidToken";
        if (!passToken.getMember().getId().equals(id))
            return "invalidToken";

        Calendar cal = Calendar.getInstance();
        if (passToken.getExpiresOn().compareTo(cal.getTime()) <= 0) {
            return "expired";
        }

        Member e = passToken.getMember();
        //
        String username = e.getUsername();
        Authentication auth = new UsernamePasswordAuthenticationToken(username, null,
                Arrays.asList(new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE")));
        SecurityContextHolder.getContext().setAuthentication(auth);
        return null;
    }

    public void send(SimpleMailMessage msg) {

        if (msg.getFrom() == null || msg.getFrom().equals(""))
            msg.setFrom(env.getMailSender());

        logger.debug("from: {}", msg.getFrom());
        logger.debug("to: {}", msg.getTo()[0]);
        logger.debug("subject: {}", msg.getSubject());
        logger.debug("text: {}", msg.getText());

        if (env.getMailEnabled())
            emailSender.send(msg);

    }

}