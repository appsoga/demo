package com.example.demo.web.security.ktp;

import java.util.Collection;

import com.example.demo.web.security.ktp.KTPUserDetailsService.LoginResponse;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@lombok.Data
public class KTPUserDetails implements UserDetails {

    private static final long serialVersionUID = -7753211995087947453L;

    String username;
    String password;
    LoginResponse loginResponse;
    Collection<GrantedAuthority> authorities;

    public KTPUserDetails(String username, String password, LoginResponse loginResponse) {
        this.username = username;
        this.password = password;
        this.loginResponse = loginResponse;
    }

    public Collection<GrantedAuthority> getAuthorities() {
        if (this.authorities == null)
            this.authorities = new java.util.ArrayList<GrantedAuthority>();
        return this.authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}