package com.example.demo.web.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

	public static final String getMemberId() {
		MemberDetails uDetails = getPrincipal();
		return uDetails == null ? "#" : uDetails.getId();
	}

	public static final MemberDetails getPrincipal() {
		com.example.demo.web.security.MemberDetails md = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		// System.out.println("#################");
		// System.out.println(auth.getPrincipal().getClass());
		// System.out.println("#################");
		if (auth.getPrincipal() instanceof com.example.demo.web.security.MemberDetails) {
			md = (com.example.demo.web.security.MemberDetails) auth.getPrincipal();
		}
		return md;
	}

}
