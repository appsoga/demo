package com.example.demo.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class StorageService {

    @lombok.Data
    public static class FileUpload {
        String personId;
        String name;

        @JsonIgnore
        MultipartFile file;
    }

    public static class StorageFileNotFoundException extends Exception {
        private static final long serialVersionUID = 1L;

    }

    private Logger logger = LoggerFactory.getLogger(StorageService.class);

    public void init() {

    }

    public void store(final FileUpload upload) throws StorageFileNotFoundException {
        if (upload.getPersonId().equals("444"))
            throw new StorageFileNotFoundException();

        logger.info(upload.toString());
        logger.info("originalName: " + upload.getFile().getOriginalFilename());
    }

    public void store(MultipartFile file) throws IOException {
        logger.info("file: {}", file.getOriginalFilename());

        InputStream inputStream = file.getInputStream();
        String originalName = file.getOriginalFilename();
        String name = file.getName();
        String contentType = file.getContentType();
        long size = file.getSize();
        logger.info("inputStream: " + inputStream);
        logger.info("originalName: " + originalName);
        logger.info("name: " + name);
        logger.info("contentType: " + contentType);
        logger.info("size: " + size);

    }

    public Stream<Path> loadAll() {
        return null;
    }

    public Path load(String filename) {
        return null;
    }

    public Resource loadAsResource(String filename) {

        return null;
    }

    void deleteAll() {

    }

}