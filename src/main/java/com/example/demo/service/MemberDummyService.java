package com.example.demo.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.example.demo.web.api.model.ApiPagingRequest;
import com.example.demo.web.api.model.ApiPagingRequest.Query;
import com.example.demo.web.api.model.ApiResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class MemberDummyService {

    @lombok.Data
    @lombok.EqualsAndHashCode(of = "id")
    @JsonIgnoreProperties(value = { "password" })
    public static class Member {
        public Member() {
            ;
        }

        public Member(Integer id) {
            this.id = id;
        }

        private Integer id;
        private String username;
        private String password;
        private String group;
        private Date expiresOn;
        private Date lastAccessedOn;
        private Boolean locked;
        private Boolean enabled;
        private String name;
        private String email;
        private Date modifiedOn;
        private Integer worker;
    }

    private static final int TOTALCNT = 1024;

    private static List<Member> mdb;

    private static int lastId;

    static {
        mdb = new ArrayList<Member>();
        for (int i = 1; i < TOTALCNT; i++) {
            mdb.add(createMember(i));
        }
        lastId = mdb.size();
    }

    private static Member createMember(int i) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Member e1 = new Member();
        e1.setId(i);
        e1.setUsername(String.format("username%d", i));
        e1.setGroup((i % 5 == 3 ? "A" : "O"));
        e1.setExpiresOn(cal.getTime());
        e1.setLastAccessedOn(Calendar.getInstance().getTime());
        e1.setLocked(i % 3 == 0 ? true : false);
        e1.setName(String.format("name%d", i));
        e1.setEmail(String.format("user%d@mail.com", i));
        e1.setModifiedOn(Calendar.getInstance().getTime());
        e1.setEnabled((i % 3 == 1 ? true : false));
        e1.setWorker(1);
        return e1;
    }

    public ApiResponse findAll(ApiPagingRequest sr) {

        /**
         * filter
         */
        java.util.List<Member> db = new java.util.ArrayList<Member>();
        if (sr.getQuerys().getQuery().size() == 0)
            db.addAll(mdb);
        else {
            for (Member e : mdb) {
                boolean flag = false;
                for (Query q : sr.getQuerys().getQuery()) {
                    String value = q.getValue();
                    if (q.getField().equals("id")) {
                        flag = e.getId().equals(Integer.valueOf(value)) ? true : false;
                    } else if (q.getField().equals("username")) {
                        flag = e.getUsername().indexOf(value) > 0 ? true : false;
                    } else if (q.getField().equals("group")) {
                        flag = e.getGroup().equals(value) ? true : false;
                        // } else if (q.getField().equals("expiresOn")) {
                        // flag = e.getGroup().equals(value) > 0 ? true : false;
                    } else if (q.getField().equals("locked")) {
                        flag = e.getLocked() == Boolean.valueOf(value) ? true : false;
                    } else if (q.getField().equals("enabled")) {
                        flag = e.getEnabled() == Boolean.valueOf(value) ? true : false;
                    } else if (q.getField().equals("name")) {
                        flag = e.getName().indexOf(value) > 0 ? true : false;
                    } else if (q.getField().equals("email")) {
                        flag = e.getEmail().indexOf(value) > 0 ? true : false;
                    }
                }
                if (flag)
                    db.add(e);
            }
        }

        /**
         * sorting
         */
        if (sr.getSort() != null) {
            
            java.util.Collections.sort(db, new java.util.Comparator<Member>() {

                @Override
                public int compare(Member o1, Member o2) {
                    if (sr.getSort().getOrder().equalsIgnoreCase("asc")) {
                        if (sr.getSort().getField().equals("id"))
                            return o1.getId().compareTo(o2.getId());
                        else if (sr.getSort().getField().equals("username"))
                            return o1.getUsername().compareTo(o2.getUsername());
                        else if (sr.getSort().getField().equals("name"))
                            return o1.getName().compareTo(o2.getName());
                        else if (sr.getSort().getField().equals("email"))
                            return o1.getEmail().compareTo(o2.getEmail());
                    } else {
                        if (sr.getSort().getField().equals("id"))
                            return o2.getId().compareTo(o1.getId());
                        else if (sr.getSort().getField().equals("username"))
                            return o2.getUsername().compareTo(o1.getUsername());
                        else if (sr.getSort().getField().equals("name"))
                            return o2.getName().compareTo(o1.getName());
                        else if (sr.getSort().getField().equals("email"))
                            return o2.getEmail().compareTo(o1.getEmail());
                    }
                    return 0;
                }

            });
        }

        /*
         * 가상의 결과값을 생성한다.
         */
        List<Member> operatorList = new ArrayList<Member>();
        long totalCount = Long.valueOf(db.size());

        int pageIndex = sr.getWebApi().getPaging().getPageIndex();
        int lineCnt = sr.getWebApi().getPaging().getLineCount();

        int index = ((pageIndex - 1) * lineCnt);
        long limit = pageIndex * lineCnt;
        limit = ((limit < totalCount) ? limit : totalCount) - 1;
        for (int i = index; i <= limit; i++) {
            operatorList.add(db.get(i));
        }
        //
        return ApiResponse.create(operatorList, totalCount);
    }

    public Member findMember(Member e1) {
        int index = mdb.indexOf(e1);
        return mdb.get(index);
    }

    public Member createMember(Member e1) {
        e1.setId(++lastId);
        mdb.add(e1);
        return e1;
    }

    public Member modifyMember(Member e1) {
        Member mr = findMember(e1);
        BeanUtils.copyProperties(e1, mr, "id", "username", "password");
        return mr;
    }

    public void removeMember(Member e1) {
        mdb.remove(e1);
    }

    public void removeMember(Integer id) {
        mdb.remove(new Member(id));
    }

}