package com.example.demo.service;

import java.util.Calendar;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import com.example.demo.data.Member;
import com.example.demo.data.MemberGroup;
import com.example.demo.data.repository.MemberRepository;
import com.example.demo.data.specs.MemberSpecs;
import com.example.demo.web.services.EnvironmentProperties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import sangmok.util.jsgrid.JsGridPageRequest;
import sangmok.util.jsgrid.JsGridRequest;
import sangmok.util.jsgrid.JsGridResponse;
import sangmok.util.jsgrid.JsGridSpecificationFactory;

@Service
public class MemberService implements InitializingBean {

	private static final int SAMPLE_DATA_COUNT = 23;

	private static Logger logger = LoggerFactory.getLogger(MemberService.class);

	@Autowired(required = false)
	private PasswordEncoder passwordEncoder;

	@Autowired(required = false)
	private EnvironmentProperties env;

	@Autowired
	private MemberRepository memberRepository;

	public Member getMemberByUsername(String username) {
		logger.debug("parameter: username is {}", username);
		return memberRepository.findOneByUsername(username);
	}

	public Member createMember(Member e1) {
		if (e1 == null) {
			logger.debug("entity is must not null.");
			return null;
		}
		return memberRepository.saveAndFlush(e1);
	}

	public Member createMember(String username, String password) {
		if (username == null) {
			logger.debug("username is must not null.");
			return null;
		}
		// create Member object
		Member e1 = new Member();
		e1.setUsername(username);
		e1.setPassword(encode(password));
		e1.setGroup(MemberGroup.USER);
		e1.setName(username);
		e1.setLocked(false);
		e1.setEnabled(false);
		e1.setLastModifiedOn(new java.util.Date());
		return createMember(e1);
	}

	public Member modifyMember(Member e1) {
		logger.info("param e1: {}", e1);
		if (e1 == null || e1.getId() == null) {
			logger.debug("entity is must not null.");
			return null;
		}
		e1.setLastModifiedOn(new java.util.Date()); // 최종수정일자 변경
		Member read1 = this.getMember(e1.getId());
		if (read1 == null) {
			logger.debug("not found member: {}", e1);
			return null;
		}
		return memberRepository.saveAndFlush(copyForUpdate(read1, e1));
	}

	public void removeMemberByUsername(String username) {
		if (username == null) {
			logger.debug("username is must not null.");
			return;
		}
		Member reade1 = memberRepository.findOneByUsername(username);
		if (reade1 == null) {
			logger.debug("not found by {}.", username);
			return;
		}
		memberRepository.delete(reade1);
	}

	private String encode(String raw) {
		if (passwordEncoder == null)
			passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		return passwordEncoder.encode(raw);
	}

	public Member getMember(String id) throws EntityNotFoundException {
		logger.debug("id: {}", id);
		Optional<Member> ret = memberRepository.findById(id);
		if (ret.isPresent())
			return ret.get();
		throw new EntityNotFoundException(String.format("Entity not found with {id: %d}", id));
	}

	public Member findUserByEmail(String email) {
		Optional<Member> opt = memberRepository.findOne(MemberSpecs.equalEmail(email));
		if (opt.isPresent())
			return opt.get();
		return null;
	}

	public void removeMember(String id) {
		memberRepository.deleteById(id);
	}

	public Boolean changePassword(String username, String password) {
		if (username == null || username.isEmpty())
			return false;
		if (password == null || password.isEmpty())
			return false;
		Member read1 = this.getMemberByUsername(username);
		if (read1 == null)
			return null;
		read1.setPassword(encode(password));
		read1.setPasswordModifiedOn(new java.util.Date());
		read1.setLastModifiedOn(new java.util.Date());
		memberRepository.save(read1);
		return true;
	}

	public JsGridResponse<Member> getMembersForJsGrid(JsGridRequest jsr, Member filter) {
		logger.debug("jsGrid: request is {}, filter is {}", jsr, filter);

		if (jsr == null) {
			jsr = new JsGridRequest();
			jsr.setPageIndex(1);
			jsr.setPageSize(13);
		}

		Specification<Member> specs = Specification.where(null);
		Specification<Member> specs4 = JsGridSpecificationFactory.toSpecification(filter);
		specs = Specification.where(specs).and(specs4);
		// pagleable
		JsGridPageRequest pageable = new JsGridPageRequest(jsr.getPageIndex() - 1, jsr.getPageSize(), jsr.getSort());
		Page<Member> page = memberRepository.findAll(specs, pageable);
		// jsgrid response
		JsGridResponse<Member> jtr = new JsGridResponse<Member>();
		jtr.setData(page.getContent());
		jtr.setItemsCount(page.getTotalElements());
		return jtr;
	}

	public static Member copyForUpdate(Member t, Member s) {
		// t.setUsername(s.getUsername());
		// t.setPassword(s.getPassword());
		t.setGroup(s.getGroup());
		t.setName(s.getName());
		t.setEmail(s.getEmail());
		if (s.getEnabled() != null)
			t.setEnabled(s.getEnabled());
		t.setExpiresOn(s.getExpiresOn());
		t.setLastModifiedOn(s.getLastModifiedOn());
		return t;
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		if (memberRepository.count() > 0)
			return;

		for (int i = 1; i <= SAMPLE_DATA_COUNT; i++) {
			Member e1 = new Member();
			e1.setUsername(String.format("user%d", i));
			e1.setPassword(encode("password"));
			e1.setGroup(MemberGroup.USER);
			e1.setName(String.format("user%d", i));
			e1.setEmail(String.format("user%d@xxx.com", i));
			e1.setExpiresOn(Calendar.getInstance().getTime());
			e1.setLocked(false);
			e1.setEnabled(false);
			e1.setLastModifiedOn(Calendar.getInstance().getTime());
			createMember(e1);
		}
		logger.info("add sample users to db.");

		Member admin = createMember(env.getAdmin().getUsername(), env.getAdmin().getPassword());
		admin.setGroup(MemberGroup.ADMIN);
		admin.setEmail("appsoga@gmail.com");
		admin.setLocked(false);
		admin.setEnabled(true);
		memberRepository.save(admin);
		logger.info("add admin user to db.");

	}

}