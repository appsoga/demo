package com.example.demo.service;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.example.demo.data.OrgMember;
import com.example.demo.data.OrgPosition;
import com.example.demo.data.Organization;
import com.example.demo.data.repository.OrgMemberRepository;
import com.example.demo.data.repository.OrgPositionRepository;
import com.example.demo.data.repository.OrganizationRepository;
import com.example.demo.data.specs.OrganizationSpecs;
import com.example.demo.web.api.model.ApiPagingRequest;
import com.example.demo.web.api.model.ApiPagingRequest.Query;
import com.example.demo.web.api.model.ApiPagingSpecificationFactory;
import com.example.demo.web.api.model.ApiResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class OrganizationService implements InitializingBean {

    private int index = 0;

    @Autowired
    private OrganizationRepository orgRepository;

    @Autowired
    private OrgPositionRepository posRepository;

    @Autowired
    private OrgMemberRepository memberRepository;

    /**
     * 페이징된 조직구성정보를 가져온다.
     * 
     * @param in
     * @return
     */
    public ApiResponse findOrganizations(ApiPagingRequest sr) {

        Specification<Organization> specs = ApiPagingSpecificationFactory.createSpecification(sr);
        for (Query data : sr.getQuerys().getQuery()) {
            if (data.getType().equals("parent")) {
                String value = data.getValue();
                if (value.equals("#")) {
                    Specification<Organization> s = OrganizationSpecs.isNullUpper();
                    specs = Specification.where(specs).and(s);
                } else {
                    Specification<Organization> s = OrganizationSpecs.equalUpperId(value);
                    specs = Specification.where(specs).and(s);
                }
            }
        }
        Page<Organization> page = orgRepository.findAll(specs, sr.getPageable());
        return ApiResponse.create(page.getContent(), page.getTotalElements());
    }

    /**
     * (C) 조직구성을 추가한다.
     * 
     * @param in
     * @return
     */
    public Organization addOrganization(Organization in) {
        if (in.getId() != null)
            return null;
        return orgRepository.save(in);
    }

    /**
     * (R) 조직구성을 가져온다.
     * 
     * @param id
     * @return
     */
    public Optional<Organization> getOrganization(Integer id) {
        return orgRepository.findById(id);
    }

    /**
     * (U) 조직구성을 수정한다.
     * 
     * @param in
     * @return
     */
    public Organization modifyOrganization(Organization in) {
        Optional<Organization> opt = this.getOrganization(in.getId());
        if (opt.isPresent()) {
            Organization read = opt.get();
            BeanUtils.copyProperties(in, read, "id", "upper");
            if (in.getUpper() != null)
                read.setUpper(in.getUpper());
            return orgRepository.save(read);
        }
        return null;
    }

    /**
     * (D) 조직구성을 삭제한다.
     * 
     * @param in
     */
    public void removeOrganization(Organization in) {
        Optional<Organization> opt = this.getOrganization(in.getId());
        if (opt.isPresent()) {
            Organization read = opt.get();
            orgRepository.delete(read);
        }
    }

    /**
     * for tree: 조직구성의 자식구성정보를 가져온다.
     * 
     * @param upperId
     * @return
     */
    public List<Organization> findOrganizationsByUpperId(Integer upperId) {
        if (upperId == null)
            return Collections.emptyList();
        return orgRepository.findByUpperId(upperId);
    }

    /************ member ***********/

    public ApiResponse findMembers(ApiPagingRequest sr) {

        Specification<OrgMember> specs = ApiPagingSpecificationFactory.createSpecification(sr);

        // Specification<OrgMember> specs = Specification.where(null);
        // for (Query data : sr.getQuerys().getQuery()) {

        // if (data.getType().equals("org")) {
        // String value = data.getValue();
        // Specification<OrgMember> s = OrgMemberSpecs.equalOrg(value);
        // specs = Specification.where(specs).and(s);
        // }
        // if (data.getType().equals("name")) {
        // String value = String.format("%%%s%%", data.getValue());
        // Specification<OrgMember> s = OrgMemberSpecs.likeName(value);
        // specs = Specification.where(specs).and(s);
        // }
        // }
        Page<OrgMember> page = memberRepository.findAll(specs, sr.getPageable());
        return ApiResponse.create(page.getContent(), page.getTotalElements());
    }

    public OrgMember getMember(String id) {
        Optional<OrgMember> ro = memberRepository.findById(id);
        if (ro.isPresent())
            return ro.get();
        return null;
    }

    public OrgMember addMember(OrgMember in) {
        return memberRepository.save(in);
    }

    public OrgMember modifyMember(OrgMember in) {
        OrgMember read = this.getMember(in.getId());
        if (read == null)
            return null;
        String[] ignoreProperties = { "id" };
        BeanUtils.copyProperties(in, read, ignoreProperties);
        return memberRepository.save(read);
    }

    public void removeMember(OrgMember in) {
        OrgMember read = this.getMember(in.getId());
        if (read != null)
            memberRepository.delete(read);
    }

    /**************************************************** */

    @Override
    public void afterPropertiesSet() throws Exception {

        if (orgRepository.count() > 0)
            return;

        Organization root = new Organization();
        root.setLevel(0);
        root.setText("APPSOGA");
        root.setOrder(0);
        root.setCompCode("COM000000");
        root = orgRepository.save(root);

        for (int i = 1; i < 8; i++) {
            Organization dept = addOrg(root, i);
            OrgPosition pos = this.addPos(dept, "CEO");
            addMember(dept, pos, i);

            for (int r = 1; r < 5; r++) {
                Organization subdept = addOrg(dept, r);
                OrgPosition subpos = this.addPos(dept, "사장");
                addMember(subdept, subpos, i);
            }
        }

    }

    private OrgPosition addPos(Organization org, String name) {
        OrgPosition pos = new OrgPosition();
        pos.setName(name);
        pos.setOrder(++index);
        pos.setOrg(org);
        pos = posRepository.save(pos);
        return pos;
    }

    private void addMember(Organization org, OrgPosition pos, int i) {
        OrgMember m = new OrgMember();
        m.setName(String.format("Name%04d%03d", ++index, i));
        m.setPhone(String.format("010-%04d-%04d", ++index, i));
        m.setEmail(String.format("M%04d%03d@example.com", ++index, i));
        m.setPosition(pos);
        m.setStatus((i % 2 == 0 ? "Active" : "Inactive"));
        m.setLastModifiedOn(Calendar.getInstance().getTime());
        //
        m.setPttId(String.format("PTT%04d%04d", ++index, i));
        m.setFmcNo(i);
        m.setOrg(org);
        memberRepository.save(m);
    }

    private Organization addOrg(Organization upper, int i) {
        Organization dept = new Organization();
        dept.setText(String.format("ORG%04d%03d", ++index, i));
        dept.setLevel(upper.getLevel() + 1);
        dept.setOrder(i);
        dept.setUpper(upper);
        dept.setCompCode("COM000000");
        return orgRepository.save(dept);
    }

    /**************************************************** */

}