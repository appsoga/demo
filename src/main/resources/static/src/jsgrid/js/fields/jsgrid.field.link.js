(function (jsGrid, $, undefined) {

    function LinkField(config) {

        this.min = "";
        this.max = "";

        jsGrid.Field.call(this, config);
    }

    LinkField.prototype = new jsGrid.Field({

        href: "details.html",
        target: "_self",
        params: "",

        css: "link-field",
        align: "center",
        // filtering: true,
        // inserting: true,
        // editing: true,
        autosearch: true,
        readOnly: false,

        itemTemplate: function (value, item) {
            var link = this.href,
                params = this.params.split(','),
                $result = $("<a>").text(value).addClass("btn").attr("target", this.target);
            if (value) {
                params.forEach(function (e, idx) {
                    var key = e.trim();
                    link = link + (idx == 0 ? '?' : '&') + key + '=' + item[key];
                });
                return $result.attr("href", link);
            }
            return "-";
        },

        filterTemplate: function (value) {
            if (!this.filtering)
                return "";

            var grid = this._grid,
                $result = this.filterControl = this._createTextBox();

            if (this.autosearch) {
                $result.on("keypress", function (e) {
                    if (e.which === 13) {
                        grid.search();
                        e.preventDefault();
                    }
                });
            }

            return $result;
        },

        filterValue: function () {
            return this.filterControl.val();
        },

        insertTemplate: function (value) {
            if (!this.inserting)
                return "";

            return this.insertControl = this._createTextBox();
        },
        insertValue: function () {
            return this.insertControl.val();
        },

        editTemplate: function (value) {
            if (!this.editing)
                return this.itemTemplate.apply(this, arguments);

            var $result = this.editControl = this._createTextBox();
            $result.val(value);
            return $result;
        },
        editValue: function () {
            return this.editControl.val();
        },
        _createTextBox: function () {
            return $("<input>").attr("type", "text")
                .prop("readonly", !!this.readOnly);
        }

    });

    jsGrid.fields.link = jsGrid.LinkField = LinkField;

}(jsGrid, jQuery));