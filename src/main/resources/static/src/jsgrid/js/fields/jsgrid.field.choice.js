// type: choice
(function (jsGrid, $, undefined) {


    function ChoiceControlField(config) {

        this.name = "id";
        this.sorting = false;
        this.multiChoice = false;

        jsGrid.Field.call(this, config);
    }

    ChoiceControlField.prototype = new jsGrid.Field({

        choiceClass: 'jsgrid-selected',

        headerTemplate: function () {

            var grid = this._grid,
                selectedItemControls = grid._selectedItemControls;

            if (this.multiChoice) {

                var $result = this.headerControl = $("<input>").attr("type", "checkbox").addClass("select-all-appsoga");

                $result.on("change", function (e) {
                    var val = this.checked;
                    $(selectedItemControls).each(function (idx, el) {
                        el.prop("checked", !val);
                        el.click();
                    });
                });

                return $result;
            } else
                return "";
        },

        itemTemplate: function (_, item) {
            var grid = this._grid,
                choiceClass = this.choiceClass,
                selectedItemControls = grid._selectedItemControls,
                selectedItems = grid._selectedItems;

            if (this.multiChoice) { // multi choice
                var css = this.choiceClass;
                var doSelect = this._selectItem;
                var doUnselect = this._unselectItem;

                var $result = $("<input>").attr("type", "checkbox")
                    .attr("name", 'choice-' + (item[this.name] ? item[this.name] : 0))
                    .attr("data-primary-key", item[this.name] ? item[this.name] : 0)
                    .prop("checked", false);

                $result.click(function (e) {
                    var val = $(this).prop("checked");
                    var td = $(this).parent().get(0);
                    var tr = $(td).parent().get(0);
                    if (val) { // selecte item
                        if (!selectedItems.includes(item)) selectedItems.push(item);
                        $(tr).addClass(css);
                    } else { // unselect item
                        var index = selectedItems.indexOf(item);
                        if (index != -1) selectedItems.splice(index, 1);
                        $(tr).removeClass(css);
                    }
                });
            } else {  // single choice
                var $result = $("<input>").attr("type", "radio").attr("name", "choice")
                    // .attr("value", item[this.name] ? item[this.name] : 0)
                    .attr("data-primary-key", item[this.name] ? item[this.name] : 0)
                    .prop("checked", false);

                $result.click(function (e) {
                    var td = $(this).parent().get(0);
                    var tr = $(td).parent().get(0);
                    // console.log(this);
                    // single choice: 기존 선택 아이템을 제거하고, 새로 선택된 것을 기록한다.
                    selectedItems.forEach(function (i, k) {
                        // console.log(i, k);
                        var idx = selectedItems.indexOf(item);
                        if (idx == -1) selectedItems.splice(idx, 1);
                    });
                    if (!selectedItems.includes(item)) selectedItems.push(item);
                    // 기존에 선택된 row의 색을 제거하고 선택된 row의 색을 지정한다.
                    grid._content.find("tr").each(function (e) {
                        $(this).removeClass(choiceClass);
                    });
                    $(tr).addClass(choiceClass);
                });
            }
            selectedItemControls.push($result);
            return $result;
        },

        width: 10,
        css: "data-field",
        align: "center",
        // format: "yy. mm. dd",
        sorting: false,
        filtering: false,
        inserting: false,
        editing: false,
        autosearch: false,
        readOnly: false

    });

    jsGrid.fields.choice = jsGrid.ChoiceControlField = ChoiceControlField;

}(jsGrid, jQuery));