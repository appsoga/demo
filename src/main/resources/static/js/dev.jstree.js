/**
 * 트리구조 주소록
 */
var tree,
    selectedNode;

$(document).ready(function () {

    // $.jstree.root = "1";  // 시작 루트를 지정한다.

    var orgRootNodeId = "1";

    var $orgTree = $("#orgTree1").bind('create_node.jstree', function (e, data) {
        var original = data.node.original;
        delete original.id;
        $.ajax({
            "type": "POST",
            "url": "../api/organizations",
            "contentType": "application/json",
            // "headers": {},
            "crossDomain": true,
            "dataType": "json",
            "data": JSON.stringify(original),
            "beforeSend": function (xhr, opts) {
                var data = { "webApi": { "data": JSON.parse(opts.data) } }
                opts.data = JSON.stringify(data);
                // console.log(data, opts.data);
                // if (false) {
                //     xhr.abort();
                // }
            },
            "dataFilter": function (data, type) {
                var obj = JSON.parse(data);
                return JSON.stringify(obj.webApi.data);
            },
            "success": function (node, textStatus, jqXHR) {
                tree.set_id(data.node, node.id);
                tree.edit(data.node);
            }
        }).done(function () {
            console.log('done');
        });
    }).bind('rename_node.jstree', function (e, data) {
        var original = data.node.original;
        original['id'] = data.node.id;
        original['text'] = data.node.text;
        console.log(original);
        $.ajax({
            "type": "PUT",
            "url": "../api/organizations",
            "contentType": "application/json",
            // "headers": {},
            "crossDomain": true,
            "dataType": "json",
            "data": JSON.stringify(original),
            "beforeSend": function (xhr, opts) {
                var data = { "webApi": { "data": JSON.parse(opts.data) } }
                opts.data = JSON.stringify(data);

                // console.log(opts.data);
                // if (false) {
                //     xhr.abort();
                // }
            },
            "dataFilter": function (data, type) {
                var obj = JSON.parse(data);
                return JSON.stringify(obj.webApi.data);
            },
            "success": function (node, textStatus, jqXHR) {
                // tree.set_id(data.node, node.id);
                // tree.edit(data.node);
            }
        }).done(function () {
            console.log('done');
        });
    }).bind('delete_node.jstree', function (e, data) {
        // console.log(data);
        var original = data.node;
        $.ajax({
            "type": "DELETE",
            "url": "../api/organizations",
            "contentType": "application/json",
            // "headers": {},
            // "async": false,
            // "crossDomain": true,
            "dataType": "json",
            "data": JSON.stringify({ "webApi": { "data": { "id": original.id } } }),
            // "beforeSend": function (xhr, opts) {
            //     console.log(opts.data);
            //     if (false) {
            //         xhr.abort();
            //     }
            // },
            "dataFilter": function (data, type) {
                var obj = JSON.parse(data);
                return JSON.stringify(obj.webApi);
            },
            "success": function (data, textStatus, jqXHR) {

            },
            "statusCode": {
                404: function () {
                    alert("page not found");
                },
                500: function () {
                    alert('error occurrence')
                }
            },
            "error": function (jqHXR, textStatus, errorThrown) {
                // console.log(jqHXR);
                if (jqHXR.status == 400) {
                    var parent = tree.get_node(original.parent);
                    tree.refresh_node(parent);
                    var data = jqHXR.responseJSON.webApi;
                    alert(data.message);
                    // tree.refresh();
                }
            }
        }).done(function () {
            console.log('done');
        });

    }).bind('before.jstree', function (e, data) {
        // invoked before jstree starts loading
    }).bind('loaded.jstree', function (e, data) {
        // invoked after jstree has loaded
        data.instance.open_node(orgRootNodeId);
        // $(this).jstree(true).close_all();
        // $(this).jstree(true).open_all();
    }).bind("changed.jstree", function (e, data) {
        // console.log(e, data);
        switch (data.action) {
            case 'deselect_all':
                break;
            case 'select_node':
                // console.log(data);
                var path = data.instance.get_path(data.node, '/');
                $("#orgPath").text(path);
                $("#jstree-node").text(JSON.stringify(data.node.original));
                selectedNode = data.node;

                // 선택된 조직의 아이디를 그리드에서 사용하도록 설정
                orgId = data.node.id;
                gridMembers.jsGrid("loadData");
                break;
        }

        // if (data.selected.length) {
        //     alert('The selected node is: ' + data.instance.get_node(data.selected[0]).text);
        // }

    }).jstree({
        core: {
            "check_callback": function (operation, node, node_parent, node_position, more) {
                // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node', 'copy_node' or 'edit'
                // in case of 'rename_node' node_position is filled with the new node name
                switch (operation) {
                    case 'edit':
                        // if (node.parent == '#')
                        //     return false;
                    case 'create_node':
                    case 'rename_node':
                        return true;
                    case 'delete_node':
                        console.log(node);
                        if (node.parent === '#')
                            return false;
                        return true;
                    default:
                        false;
                }
            },
            "data": {
                "method": "POST",
                "url": function (node) {
                    if (node.id === '#')
                        return "../api/organizations/tree/root";
                    else
                        return "../api/organizations/tree/childs";
                },
                "contentType": "application/json",
                "crossDomain": true,
                // "headers": {},
                "dataType": "json",
                "data": function (node) {
                    // console.log(node);
                    var data = {
                        "webApi": {
                            "paging": {
                                "pageIndex": 1,
                                "lineCount": 1000,
                                "querys": {
                                    "query": [
                                        { "type": "parent", "value": (node.id === '#') ? orgRootNodeId : node.id }
                                    ]
                                }
                            }
                        }
                    };
                    // console.log(JSON.stringify(data));
                    return JSON.stringify(data);
                },
                // "beforeSend": function (xhr, opts) {
                //     // var data = { "webApi": { "data": JSON.parse(opts.data) } }
                //     // opts.data = JSON.stringify(data);
                //     // console.log(opts);
                //     if (false) {
                //         xhr.abort();
                //     }
                // },
                "dataFilter": function (data, type) {
                    // console.log(data);
                    var obj = JSON.parse(data);
                    return JSON.stringify(obj.webApi.list);
                },
                "success": function (data, textStatus, jqXHR) {
                    // console.log(data);
                },
                "statusCode": {
                    404: function () {
                        alert("page not found");
                    }
                },
                "error": function (jqHXR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            }
        },
        "types": {
            "#": { "max_children": 5, "max_depth": 5, "valid_children": ["root"] },
            "root": { "icon": "glyphicon glyphicon-book", "valid_children": ["default"] },
            "default": { "icon": "glyphicon glyphicon-tags", "valid_children": ["default", "file"] },
            "file": { "icon": "glyphicon glyphicon-file", "valid_children": [] }
        },
        "plugins": [
            // "contextmenu",
            // "dnd",
            // "search",
            // "state", 
            // "wholerow",
            "types"
        ],
        // "contextmenu": {
        //     "items": function ($node) {
        //         var tree = $orgTree.jstree(true);
        //         return {
        //             "Create": {
        //                 "separator_before": false,
        //                 "separator_after": true,
        //                 "label": "Create",
        //                 "action": false,
        //                 "submenu": {
        //                     "File": {
        //                         "seperator_before": false,
        //                         "seperator_after": false,
        //                         "label": "File",
        //                         action: function (obj) {
        //                             $node = tree.create_node($node, {
        //                                 text: 'New File',
        //                                 type: 'file',
        //                                 icon: 'glyphicon glyphicon-file'
        //                             });
        //                             tree.deselect_all();
        //                             tree.select_node($node);
        //                         }
        //                     },
        //                     "Folder": {
        //                         "seperator_before": false,
        //                         "seperator_after": false,
        //                         "label": "Folder",
        //                         action: function (obj) {
        //                             $node = tree.create_node($node, {
        //                                 text: 'New Folder',
        //                                 type: 'default'
        //                             });
        //                             tree.deselect_all();
        //                             tree.select_node($node);
        //                         }
        //                     }
        //                 }
        //             },
        //             "Test": {
        //                 "separator_before": false,
        //                 "separator_after": false,
        //                 "label": "Test",
        //                 "action": function (obj) {
        //                     $orgTree.jstree('deselect_all');
        //                 }
        //             },
        //             "Rename": {
        //                 "separator_before": false,
        //                 "separator_after": false,
        //                 "label": "Rename",
        //                 "action": function (obj) {
        //                     tree.edit($node);
        //                 }
        //             },
        //             "Remove": {
        //                 "separator_before": false,
        //                 "separator_after": false,
        //                 "label": "Remove",
        //                 "action": function (obj) {
        //                     tree.delete_node($node);
        //                 }
        //             }
        //         };
        //     }
        // }
    })
        // .on("init.jstree", function () { } // triggered after all events are bound 
        // .on("loading.jstree", function () { } // triggered after the loading text is shown and before loading starts 
        // .on("destroy.jstree", function () {}// triggered before the tree is destroyed 
        // .on("loaded.jstree", function () {} // triggered after the root node is loaded for the first time 
        // .on("ready.jstree", function () {} // triggered after all nodes are finished loading 
        // .on("load_node.jstree", function (node, status) {} // triggered after a node is loaded 
        // .on("load_all.jstree", function (node) {} // triggered after a load_all call completes 
        // .on("model.jstree", function (nodes, parent) {} // triggered when new data is inserted to the tree model 
        // .on("redraw.jstree", function (nodes) {}// triggered after nodes are redrawn 
        // .on("before_open.jstree", function (node) { }// triggered when a node is about to be opened (if the node is supposed to be in the DOM, it will be, but it won't be visible yet) 
        // .on("open_node.jstree", function (node) {} // triggered when a node is opened (if there is an animation it will not be completed yet) 
        // .on("after_open.jstree", function (node) {} // triggered when a node is opened and the animation is complete 
        // .on("close_node.jstree", function (node) {} // triggered when a node is closed (if there is an animation it will not be complete yet) 
        // .on("after_close.jstree", function (node) {} // triggered when a node is closed and the animation is complete 
        // .on("open_all.jstree", function (node) {} // triggered when an open_all call completes 
        // .on("close_all.jstree", function (node) {} // triggered when an close_all call completes 
        // .on("enable_node.jstree", function (node) {} // triggered when an node is enabled 
        // .on("disable_node.jstree", function (node) {} // triggered when an node is disabled 
        // .on("hide_node.jstree", function (node) {} // triggered when an node is hidden 
        // .on("show_node.jstree", function (node) {} // triggered when an node is shown 
        // .on("hide_all.jstree", function (nodes) {} // triggered when all nodes are hidden 
        // .on("show_all.jstree", function (nodes) {} // triggered when all nodes are shown 
        // .on("activate_node.jstree", function (node) {node, event} // triggered when an node is clicked or intercated with by the user 
        // .on("hover_node.jstree", function (node) {} // triggered when an node is hovered 
        // .on("dehover_node.jstree", function (node) {} // triggered when an node is no longer hovered 
        // .on("select_node.jstree", function (node, selected, event) {} // triggered when an node is selected 
        // .on("changed.jstree", function (node, action, selected, event) {} // triggered when selection changes
        // .on("deselect_node.jstree", function (node,selected, event) {} // triggered when an node is deselected 
        // .on("select_all.jstree", function (selected) {} // triggered when all nodes are selected 
        // .on("deselect_all.jstree", function (node, selected) {} // triggered when all nodes are deselected 
        // .on("set_state.jstree", function () {} // triggered when a set_state call completes 
        // .on("refresh.jstree", function (nodes) {} // triggered when a refresh call completes 
        // .on("refresh_node.jstree", function (node) {} // triggered when a node is refreshed 
        // .on("set_id.jstree", function (node, old) {} // triggered when a node id value is changed 
        // .on("set_text.jstree", function (obj, text) {} // triggered when a node text value is changed 
        // .on("create_node.jstree", function (node, parent, position) {} // triggered when a node is created 
        // .on("rename_node.jstree", function (node, text, old) {} // triggered when a node is renamed 
        // .on("delete_node.jstree", function (node, parent) {} // triggered when a node is deleted 
        // .on("move_node.jstree", function (node, parent, position, old_parent, old_position, is_multi, old_instance, new_instance) {} // triggered when a node is moved 
        // .on("copy_node.jstree", function (node, original, parent, position, old_parent, old_position, is_multi, old_instance, new_instance) {} // triggered when a node is copied
        // .on("cut.jstree", function (node) {} // triggered when nodes are added to the buffer for moving 
        // .on("copy.jstree", function (node) {} // triggered when nodes are added to the buffer for copying 
        // .on("paste.jstree", function (parent, node, mode) {} // triggered when paste is invoked 
        // .on("clear_buffer.jstree", function () {} // triggered when the copy / cut buffer is cleared 
        // .on("set_theme.jstree", function (theme) {} // triggered when a theme is set 
        // .on("show_stripes.jstree", function () {} // triggered when stripes are shown 
        // .on("hide_stripes.jstree", function () {} // triggered when stripes are hidden 
        // .on("show_dots.jstree", function () {} // triggered when dots are shown 
        // .on("hide_dots.jstree", function () {} // triggered when dots are hidden 
        // .on("show_icons.jstree", function () {} // triggered when icons are shown 
        // .on("hide_icons.jstree", function () {} // triggered when icons are hidden 
        // .on("show_ellipsis.jstree", function () {} // triggered when ellisis is shown 
        // .on("hide_ellipsis.jstree", function () {} // triggered when ellisis is hidden 

        // **** plugin event ***** 
        // .on("changed.jstree", function (node, action, selected, event) {} // changed : triggered when selection changes 
        // .on("disable_checkbox.jstree", function (node) {} // checkbox : triggered when an node's checkbox is disabled 
        // .on("enable_checkbox.jstree", function (node) {} // checkbox : triggered when an node's checkbox is enabled 
        // .on("check_node.jstree", function (node, selected, event) {} // checkbox : triggered when an node is checked (only if tie_selection in checkbox settings is false) 
        // .on("uncheck_node.jstree", function (node, selected, event) {} // checkbox : triggered when an node is unchecked (only if tie_selection in checkbox settings is false) 
        // .on("check_all.jstree", function (selected) {} // checkbox : triggered when all nodes are checked (only if tie_selection in checkbox settings is false) 
        // .on("uncheck_all.jstree", function (node, selected) {} // checkbox : triggered when all nodes are unchecked (only if tie_selection in checkbox settings is false) 
        // .on("show_contextmenu.jstree", function (node, x, y) {} // contextmenu : triggered when the contextmenu is shown for a node 
        // .on("search.jstree", function (nodes, str, res) {} // search : triggered after search is complete 
        // .on("clear_search.jstree", function (nodes, str, res) {} // search : triggered after search is complete
        //   .on("state_ready.jstree", function (node) {} // state : triggered when the state plugin is finished restoring the state (and immediately after ready if there is no state to restore). 
        .on("select_node.jstree", function (event, data) {
            // 노드가 선택된 뒤 처리할 이벤트 
            var id = data.instance.get_node(data.selected).id;
            // console.log("data.node : " + JSON.stringify(data.node));
            // 선택한 Node에 따라 하위 목록 가져오기 
            // fn_Common.jstreeDynamic(data.node.id);
        });

    tree = $orgTree.jstree(true);

});
