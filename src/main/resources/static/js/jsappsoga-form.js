/*!
 * jsForm 이건 뭐 필요해서 만들어 봤음. 현재 평면형만 지원.
 *
 * @author sangmok <appsoga@gmail.com>
 * @sine 2019. 12. 11 
 * @required jquery 2.2.4, jquery.serialize-object
 * ------------------------
 * 
 * var data = {name: 'appsoga'},
 *     ff = $("#formSignIn").jsForm({});
 * ff.setObject(data);
 * 
 */
(function (window, $, undefined) {

    function jsForm($form, config) {
        // console.log($form, config);
        this._init(config);
        this.$_form = $form;
    };

    jsForm.prototype = {

        _form: $.noop,

        _init: function (config) {

        },

        reset: function () {
            var $form = this.$_form;
            $form.each(function () {
                this.reset();
            });
        },

        setObject: function (data) {
            var $form = this.$_form,
                set = this._set;

            //set
            $form.find("[name]").each(function (idx) {
                if (data[this.name] != null) {
                    set(this, data[this.name]);
                }
            });
        },

        setField: function (name, value) {
            var $form = this.$_form,
                set = this._set;

            $form.find("[name=" + name + "]").each(function (idx) {
                set(this, value);
            });
        },

        getObject: function () {
            var $form = this.$_form;
            var data = $form.serializeObject();
            $form.find("[type='checkbox']").each(function(i) {
                if (data[this.name] == null)
                    data[this.name] = false;
            });
            return data;
        },

        clear: function () {
            var $form = this.$_form;
            $form.find("[name]").each(function (i) {
                var el = this;
                switch (el.type) {
                    case 'checkbox':
                    case 'radio':
                        $(el).prop("checked", false);
                        break;
                    case 'select-one':
                        $(el).find("option:selected").each(function (i) {
                            $(this).prop("selected", false);
                        });
                        break;
                    case 'number':
                        $(el).val(0);
                    default:
                        $(el).val('');
                        break;
                }
            });
        },

        readonly: function (bVal) {
            var $form = this.$_form;
            $form.find("[name]").each(function (i) {
                var el = this;
                switch (el.type) {
                    case 'select-one':
                        if (bVal)
                            $(el).find("option").not(":selected").each(function (i) {
                                $(this).prop("disabled", bVal);
                            });
                        else
                            $(el).find("option:disabled").each(function (i) {
                                $(this).prop("disabled", bVal);
                            });
                        break;
                    default:
                        $(el).prop("readonly", bVal);
                        break;
                }
            });

        },

        disabled: function (bVal) {
            var $form = this.$_form;
            $form.find("[name]").each(function (i) {
                var el = this;
                $(el).prop("disabled", bVal);
            });
        },

        readonlyField: function (name, bVal) {
            var $form = this.$_form;
            $form.find("[name=" + name + "]").each(function (i) {
                var el = this;
                $(el).prop("readonly", bVal);
            });
        },

        disabledField: function (name, bVal) {
            var $form = this.$_form;
            $form.find("[name=" + name + "]").each(function (i) {
                var el = this;
                $(el).prop("disabled", bVal);
            });
        },



        _set: function (element, value) {
            var el = element;
            switch (el.type) {
                case 'radio':
                    if (value == true || value == false) {
                        var bval = (el.value == 'true' ? true : false);
                        if (bval == value) $(el).prop("checked", true);
                        else $(el).prop("checked", false);
                    } else {
                        if (el.value == value) $(el).prop("checked", true);
                        else $(el).prop("checked", false);
                    }
                    break;
                case 'checkbox':
                    if (value == true || value == false) {
                        if (value == true) $(el).prop("checked", true);
                        else $(el).prop("checked", false);
                    } else if (value == 1 || value == 0) {
                        if (value == 1) $(el).prop("checked", true);
                        else $(el).prop("checked", false);
                    }
                    break;
                case 'select-one':
                    $(el).val(value).prop("selected", true);
                    break;
                case 'email':
                case 'number':
                case 'text':
                case 'password':
                default:
                    el.value = value;
                    break;
            }
        },

        _print_data: function (jsonData) {
            Object.keys(data).forEach(function (key) {
                var value = jsonData[key];
                console.log(key, jsonData[key]);
            });
        }

    };

    $.fn.jsForm = function (config) {
        return new jsForm(this, config);
    };

}(window, jQuery));